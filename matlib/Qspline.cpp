#include<iostream>
#include<Qspline.hpp>
#include<Vector_class.hpp>


Qspline::Qspline(const Vector& xs, const Vector& ys)
    :   x (xs)
    ,   y (ys)
    ,   n (xs.getSize())
    ,   b (n - 1)
    ,   c (n - 1)
{
    // Sanity check on sizes of xs and ys
    if (n != y.getSize())
    {
        std::cerr << "ERROR: MISMATCHED DATA SIZE FOR QSPLINE" << std::endl;
        exit(1);
    }
    
    // Now we create p and h vectors
    Vector p(n - 1); // Gradient of linear interpolation between points
    Vector h(n - 1); // Distance between points
    for (int i = 0; i < n - 1; i++)
    {
        h[i] = x[i + 1] - x[i];
        p[i] = (y[i + 1] - y[i]) / h[i];
    }
    
    // Recursion up
    c[0] = 0; // Can be removed (default value) but makes clear relation to notes
    for (int i = 0; i < n - 2; i++)
    {
        c[i + 1] = (p[i + 1] - p[i] - c[i] * h[i]) / h[i + 1];
    }
    
    // Recursion down
    c[n - 2] /= 2;
    for (int i = n - 3; i >= 0; i--) 
    {
        c[i] = (p[i + 1] - p[i] - c[i + 1] * h[i + 1]) / h[i];
    }

    // Evaluating b from other vectors
    for (int i = 0; i < n - 1; i++) 
    {
        b[i] = p[i] - c[i] * h[i];
    }
}

Qspline::~Qspline()
{
}

double Qspline::evaluate(const double z) const
{
    int i = Qspline::binomialSearch(x, z);
    double h = x[i + 1] - x[i];
    double t = (z - x[i]) / h;
    return y[i] * (1 - t) + y[i + 1] * t + h * h / 6 * ((1 - t) * b[i] + t * b[i + 1]);
}

Vector Qspline::getB() const
{
    return b;
}

Vector Qspline::getC() const
{
    return c;
}

double Qspline::linearSplineIntegral(const Vector& xs, const Vector& ys, const double z)
{
    // First we do the easy bits. Linear intervals completely between xs[0] and z
    int idx_z = Qspline::binomialSearch(xs, z);
    int idx_low = 0;
    double area = 0.0;
    for (int idx_high = 1; idx_high <= idx_z; idx_high++)
    {
        area += ((ys[idx_low] + ys[idx_high]) / 2) * (xs[idx_high] - xs[idx_low]);
        idx_low = idx_high;
    }
    
    // Now we need to do the same but using the estimated ys[z]
    double ys_z = Qspline::linearInterpolation(xs, ys, z);
    area += ((ys[idx_low] + ys_z) / 2) * (z - xs[idx_low]);
    return area;
}

double Qspline::linearInterpolation(const Vector& xs, const Vector& ys, const double z)
{
    // First we find the index where z is
    int idx_z = Qspline::binomialSearch(xs, z);

    // Next we evaluate the gradient
    double dx = xs[idx_z + 1] - xs[idx_z];
    if (dx < 0)
    {
        std::cerr << "ERROR: NON INCREASING X AXIS IN LINEARINTERPOLATION" << std::endl;
        exit(1);
    }
    double dy = ys[idx_z + 1] - ys[idx_z];

    // Now we evaluate the return value
    return ys[idx_z] + (dy / dx) * (z - xs[idx_z]);
}

int Qspline::binomialSearch(const Vector& xs, const double z)
{
    // Sanity check on binsearch
    const int idx_end = xs.getSize() - 1;
    if (xs[0] > z || xs[idx_end] < z)
    {
        std::cerr << "ERROR: Z OUT OF RANGE IN BINSEARCH" << std::endl;
        exit(1);
    }
    
    // Actual binsearch
    int idx_low = 0;
    int idx_high = idx_end;
    int idx_mid;
    while (idx_high - idx_low > 1)
    {
        idx_mid = (idx_low + idx_high) / 2;
        z > xs[idx_mid] ? idx_low = idx_mid : idx_high = idx_mid;
    }
    
    return idx_low;
}
