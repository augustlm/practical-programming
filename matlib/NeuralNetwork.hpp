#pragma once

#include<functional>
#include<string>
#include<Vector_class.hpp>

class NeuralNetwork
{
private:
    
    // Number of neurons in the network
    const int n; 

    // Activation function
    std::function<double(const double)> activationFunction;

    // Parameters
    Vector parameters;

public:

    // Constructor
    NeuralNetwork(const int n, std::string activationFunctionName="GaussianWavelet");

    // Destructor
    ~NeuralNetwork();

    // Member functions

    // Function to train the neural network
    void train(const Vector& xs, const Vector& ys, double step=0.7, double accuracy=1e-6);

    // Function to get the response to input
    double response(const double x) const;

    // Function to get the derivative at input
    double getDerivative(const double x) const;

    // Function to get the second derivative at input
    double getSecondDerivative(const double x) const;

    // Function to get the anti-derivative at input
    double getAntiDerivative(const double x) const;

};
