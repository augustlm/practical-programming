#pragma once

class Matrix;

class Vector
{
private:
    
    // Define the size of the Vector and a pointer to the elements in a Vector
    int v_size;
    double* v_ptr_to_element;

public:

    // Defining constructors
    Vector();
    Vector(const int size);
    Vector(const double* arr, const int size);
    Vector(const Vector& other);

    // Defining destructor
    ~Vector();

    // Public member functions
    const int& getSize() const;
    void scale(int);
    void scale(double);
    void normalize();
    double normSquared() const;
    double norm() const;
    double sum() const;
    double max() const;
    static Vector randomVectorInt(const int size, const int shift=0, const int max=RAND_MAX);
    static Vector vectorWithOneValue(const int size, const double value);
    static Vector axisVector(const double start_range, const double stop_range, const double step_size=1.0);

    // Operators
    Vector operator+(const double x) const;
    Vector operator+(const int x) const;
    Vector operator+(const Vector& other) const;
    Vector operator-(const double x) const;
    Vector operator-(const int x) const;
    Vector operator-(const Vector& other) const;
    Vector operator*(const double x) const;
    Vector operator*(const int x) const;
    Vector operator/(const double x) const;
    Vector operator/(const int x) const;
    double& operator[](const int i);
    const double& operator[](const int i) const;
    Vector& operator=(const Vector& other);
    Vector& operator=(Vector&& other);
    bool operator==(const Vector& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Vector& vec);
};

// Non member functions
double dot_product(const Vector& Vector1, const Vector& Vector2);
Vector Matrix_Vector_product(const Matrix& mat, const Vector& vec);
Matrix outerProduct(const Vector& Vector1, const Vector& Vector2);
double innerProduct(const Vector& Vector1, const Vector& Vector2);
