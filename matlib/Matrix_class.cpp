#include<iostream>
#include<iomanip>
#include<cmath>
#include<ctime>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>

// Constructors

// Default constructor: initialise a 0 by 0 Matrix
Matrix::Matrix()
    :   m_num_cols (0)
    ,   m_num_rows (0)
    ,   m_ptr_to_element (nullptr)
{
}

// Constructor: given number of rows and cols, assigns those attributes and allocates a pointer to a pointer to an element (memory for a Matrix)
Matrix::Matrix(const int num_rows, const int num_cols)
    :   m_num_cols (num_cols)
    ,   m_num_rows (num_rows)
{
    m_ptr_to_element = new double*[m_num_rows];
    for (int i = 0; i < m_num_rows; i++)
    {
        m_ptr_to_element[i] = new double[m_num_cols]();
    }
}

// Copy constructor: copies all attributes from another Matrix
Matrix::Matrix(const Matrix& other)
    :   m_num_cols (other.m_num_cols)
    ,   m_num_rows (other.m_num_rows)
{
    m_ptr_to_element = new double*[m_num_rows];
    for (int i = 0; i < m_num_rows; i++)
    {
        m_ptr_to_element[i] = new double[m_num_cols];
        for (int j = 0; j < m_num_cols; j++)
        {
            m_ptr_to_element[i][j] = other[i][j];
        }
    }
}

// Destructors

// General destructor needs to delete the memomry assigned by constructor
Matrix::~Matrix()
{
    if (m_ptr_to_element != nullptr)
    {
        for (int i = 0; i < m_num_rows; i++)
        {
            delete[] m_ptr_to_element[i];
            m_ptr_to_element[i] = nullptr;
        }
        delete[] m_ptr_to_element;
        m_ptr_to_element = nullptr;
    }
}

// Public member functions

// getRows: returns the number of rows a Matrix has
int Matrix::getRows() const
{
    return m_num_rows;
}

// getCols: returns the number of cols a Matrix has
int Matrix::getCols() const
{
    return m_num_cols;
}

// scale: multiplies every element by the given factor
void Matrix::scale(const double factor)
{
    for (int i = 0; i < m_num_rows; i++)
    {
        for (int j = 0; j < m_num_cols; j++)
        {
            m_ptr_to_element[i][j] *= factor;
        }
    }
}

// identity: creates an n by n identity matrix
Matrix Matrix::identity(const int size)
{
    Matrix result(size, size);
    for (int i = 0; i < size; i++)
    {
        result[i][i] = 1.0;
    }
    return result;
}

// Makes a matrix of specified size with integers shifted with a certain max
Matrix Matrix::randomMatrixInt(const int height, const int width, const int shift, const int max)
{
    srand(time(0));
    Matrix result(height, width);
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            result[i][j] = (rand() + shift) % max;
        }
    }
    return result;
}

// Operators

// Adds the elements of 2 matrices to form a new matrix
Matrix Matrix::operator+(const Matrix& other) const
{
    if (other.getCols() != m_num_cols || other.getRows() != m_num_rows)
    {
        std::cout << "Error: dimensions mismatch in matrix addition " << std::endl;
        exit(1);
    }
    Matrix res(m_num_rows, m_num_cols);
    for (int i = 0; i < m_num_cols; i++)
    {
        for (int j = 0; j < m_num_rows; j++)
        {
            res[i][j] = m_ptr_to_element[i][j] + other[i][j];
        }
    }
    return res;
}

// [i]: returns the i'th element of Matrix
double* Matrix::operator[](const int i)
{    
    return m_ptr_to_element[i];
}

// [i] const: returns the i'th element of Matrix as const
const double* Matrix::operator[](const int i) const
{
    return m_ptr_to_element[i];
}

// Copy assignment operator
Matrix& Matrix::operator=(const Matrix& other)
{
    if (this != &other)
    {
        // Free memory of current object
        for (int i = 0; i < m_num_rows; i++)
        {
            delete[] m_ptr_to_element[i];
            m_ptr_to_element[i] = nullptr;
        }
        delete[] m_ptr_to_element;
        m_ptr_to_element = nullptr;

        // Copy data from other object
        m_num_cols = other.m_num_cols;
        m_num_rows = other.m_num_rows;

        m_ptr_to_element = new double*[m_num_rows];
        for (int i = 0; i < m_num_rows; i++)
        {
            m_ptr_to_element[i] = new double[m_num_cols];
            for (int j = 0; j < m_num_cols; j++)
            {
                m_ptr_to_element[i][j] = other[i][j];
            }
        }
    }
    return *this;
}

// Move operator
Matrix& Matrix::operator=(Matrix&& other)
{
    // Free memory of current object
    for (int i = 0; i < m_num_rows; i++)
    {
        delete[] m_ptr_to_element[i];
        m_ptr_to_element[i] = nullptr;
    }
    delete[] m_ptr_to_element;
    m_ptr_to_element = nullptr;

    // Move data from other object
    m_num_cols = other.m_num_cols;
    m_num_rows = other.m_num_rows;
    m_ptr_to_element = other.m_ptr_to_element;

    // Reset other object
    other.m_num_cols = 0;
    other.m_num_rows = 0;
    other.m_ptr_to_element = nullptr;

    return *this;
}

// Overload of comparison operator
bool Matrix::operator==(const Matrix& other) const
{
    // set tolerence
    const double tol = 1e-3;

    // Dimension check
    if (m_num_rows != other.getRows() || m_num_cols != other.getCols())
    {
        return false;
    }

    // Element check
    for (int i = 0; i < m_num_rows; i++)
    {
        for (int j = 0; j < m_num_cols; j++)
        {
            if (std::abs(m_ptr_to_element[i][j] - other[i][j]) > tol)
            {
                return false;
            }
        }
    }
    return true;
}


// Overload of the cout operator (note it is rounded to three decimal places)
std::ostream& operator<<(std::ostream& os, const Matrix& mat)
{
    const double threshold = 1e-3;
    for (int i = 0; i < mat.getRows(); i++)
    {
        for (int j = 0; j < mat.getCols(); j++)
        {
            double value = mat[i][j];
            if (std::abs(value) < threshold)
            {
                os << std::fixed << std::setprecision(3) << std::abs(value) << " ";
            }
            else
            {
                os << std::fixed << std::setprecision(3) << value << " ";
            }
        }
        os << std::endl;
    }
    return os;
}


// Non member functions

// Matrix product between two matrices
Matrix Matrix_product(const Matrix& mat_1, const Matrix& mat_2)
{
    int cols_1 = mat_1.getCols(), rows_1 = mat_1.getRows(), cols_2 = mat_2.getCols(), rows_2 = mat_2.getRows();
    if (cols_1 != rows_2)
    {
        std::cout << "Error, mismatched dimensions for Matrix multiplication. " << std::endl;
        exit(1);
    }
    Matrix product(rows_1, cols_2); //[row][col]
    for (int i = 0; i < rows_1; i++)
    {
        for (int j = 0; j < cols_2; j++)
        {
            float element = 0;
            for (int k = 0; k < cols_1; k++)
            {
                element += (mat_1[i][k] * mat_2[k][j]);
            }
            product[i][j] = element;
        }
    }
    return product;
}

// Returns the product of a Vector with a 1 row Matrix
Matrix Vector_Matrix_product(const Vector& vec, const Matrix& mat)
{
    int vec_size = vec.getSize(), mat_rows = mat.getRows();
    if (mat_rows > 1)
    {
        std::cout << "Error, mismatched dimensions for Vector Matrix multiplication. " << std::endl;
        exit(1);
    }
    Matrix temp_Matrix(1, vec_size);
    for (int i = 0; i < vec_size; i++)
    {
        temp_Matrix[0][i] = vec[i];
    }
    return Matrix_product(temp_Matrix, mat);
}

// Returns a new Matrix which is the transpose of the old Matrix
Matrix transpose(const Matrix& mat)
{
    const int trans_rows = mat.getCols(), trans_cols = mat.getRows();
    Matrix result(trans_rows, trans_cols);
    for (int i = 0; i < trans_rows; i++)
    {
        for (int j = 0; j < trans_cols; j++)
        {
            result[i][j] = mat[j][i];
        }
    }
    return result;
}
