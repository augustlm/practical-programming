#pragma once

#include<functional>

class Vector;
class Matrix;

class Roots
{
    public:
        static Vector newtonBacktracking
            (   const std::function<const Vector (const Vector&)>& f        /* Function we want to find roots of */
            ,   const Vector& x                                             /* Starting guess */
            ,   const double epsilon = 1e-4                                 /* Accuracy goal */
            );
        
        static Matrix finiteDifferenceJacobian
            (   const std::function<const Vector (const Vector&)>& f        /* Function we want to find the jacobian */
            ,   const Vector& x                                             /* Current guess */
            );
};