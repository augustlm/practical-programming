#include<iostream>
#include<iomanip>
#include<cmath>
#include<ctime>
#include<stdexcept>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>

// Constructors

// Default constructor: initialise a 0 length Vector
Vector::Vector()
    :   v_size (0)
    ,   v_ptr_to_element (nullptr)
{
}

// Constructor: given size, assigns that attribute and allocate a pointer to element (memory for a Vector)
Vector::Vector(const int size)
    :   v_size (size)
{
    v_ptr_to_element = new double[size]();
}

// Constructor: given an array of doubles, assigns size and copies elements into vector
Vector::Vector(const double* arr, const int size)
    :   v_size(size)
{
    v_ptr_to_element = new double[size];
    for (int i = 0; i < size; i++)
    {
        v_ptr_to_element[i] = arr[i];
    }
}


// Copy constructor: copies all attributes from another Vector
Vector::Vector(const Vector& other)
    :   v_size (other.v_size)
{
    v_ptr_to_element = new double[v_size];
    for (int i = 0; i < v_size; i++)
    {
        v_ptr_to_element[i] = other[i];
    }
    
}

// Destructors

// General destructor needs to delete the memory assigned by constructor
Vector::~Vector()
{
    if (v_ptr_to_element != nullptr)
    {
        delete[] v_ptr_to_element;
        v_ptr_to_element = nullptr;
    }
}

// Public member functions

// getSize: returns the size of a vector
const int& Vector::getSize() const
{
    return v_size;
}

// Scale int: multiply every element of the Vector by some int
void Vector::scale(int factor)
{
    for (int i = 0; i < v_size; i++)
    {
        v_ptr_to_element[i] *= factor;
    }
    
}

// Scale double: overload for scaling by doubles
void Vector::scale(double factor)
{
    for (int i = 0; i < v_size; i++)
    {
        v_ptr_to_element[i] *= factor;
    }
}

// Normalise: Divides every element by the norm
void Vector::normalize()
{
    const double factor = 1 / this->norm();
    this->scale(factor);
}

// Norm squared: returns the norm squared of the Vector
double Vector::normSquared() const
{
    double length = 0.0;
    for (int i = 0; i < v_size; i++)
    {
        length += v_ptr_to_element[i] * v_ptr_to_element[i];
    }
    return length;
}

// Norm: returns the norm of the Vector
double Vector::norm() const
{
    return sqrt(this->normSquared());
}

// Returns the sum of the elements in the vector
double Vector::sum() const
{
    double ans = 0.0;
    for (int i = 0; i < v_size; i++)
    {
        ans += v_ptr_to_element[i];
    }
    return ans;
}

// Returns the max
double Vector::max() const
{
    double res = v_ptr_to_element[0];
    for (int i = 1; i < v_size; i++)
    {
        if (v_ptr_to_element[i] > res)
        {
            res = v_ptr_to_element[i];
        }
    }
    return res;
}

// Returns a vector of specified sized with numbers within a range
Vector Vector::randomVectorInt(const int size, const int shift, const int max)
{
    srand(time(0));
    Vector answer(size);
    for (int i = 0; i < size; i++)
    {
        answer[i] = (rand() + shift) % max;
    }
    return answer;
}

// Returns a vector filled with specified value
Vector Vector::vectorWithOneValue(const int size, const double value)
{
    Vector ans(size);
    for (int i = 0; i < size; i++)
    {
        ans[i] = value;
    }
    return ans;
}

// Returns a vector containing a vector of linearly spaced points between start and finish with step size
Vector Vector::axisVector(const double start_range, const double stop_range, const double step_size)
{
    int size = std::ceil((stop_range - start_range) / step_size);
    Vector result(size);
    double current = start_range;
    for (int i = 0; i < size; i++)
    {
        result[i] = current;
        current += step_size;
    }
    return result;
}

// Operators

// +: adds a double to each element
Vector Vector::operator+(const double x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] = v_ptr_to_element[i] + x;
    }
    return ans;
}

// +: adds a int to each element
Vector Vector::operator+(const int x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] += x;
    }
    return ans;
}

// +: adds together the elements in two vectors and returns a new one
Vector Vector::operator+(const Vector& other) const
{
    if (v_size != other.getSize())
    {
        std::cerr << "ERROR: MISMATCHED SIZE FOR VECTOR ADDITION" << std::endl;
        exit(1);
    }
    Vector ans(other);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] += v_ptr_to_element[i];
    }
    return ans;
}

// -: subtracts a double to each element
Vector Vector::operator-(const double x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] -= x;
    }
    return ans;
}

// -: subtracts a int to each element
Vector Vector::operator-(const int x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] -= x;
    }
    return ans;
}

// -: subtracts together the elements in two vectors and returns a new one
Vector Vector::operator-(const Vector& other) const
{
    if (v_size != other.getSize())
    {
        std::cerr << "ERROR: MISMATCHED SIZE FOR VECTOR SUBTRACTION" << std::endl;
        exit(1);
    }
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] -= other[i];
    }
    return ans;
}

// *: multiplies each element by a double
Vector Vector::operator*(const double x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] *= x;
    }
    return ans;
}

// *: multiplies each element by a int
Vector Vector::operator*(const int x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] *= x;
    }
    return ans;
}

// /: divides each element by a double
Vector Vector::operator/(const double x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] /= x;
    }
    return ans;
}

// /: divides each element by a int
Vector Vector::operator/(const int x) const
{
    Vector ans(*this);
    for (int i = 0; i < v_size; i++)
    {
        ans[i] /= x;
    }
    return ans;
}

// [i]: returns the i'th element of Vector
double& Vector::operator[](const int i)
{
    if (i<0 || i >= v_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return v_ptr_to_element[i];
}

// [i] const: returns the i'th element of Vector as a const
const double& Vector::operator[](const int i) const
{
    if (i<0 || i >= v_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return v_ptr_to_element[i];
}

// Copy assignment operator
Vector& Vector::operator=(const Vector& other)
{
    if (this != &other)
    {
        // Free the memory allocated for this Vector
        delete[] v_ptr_to_element;
        // Copy the size attribute from the other Vector
        v_size = other.v_size;
        // Allocate new memory for this Vector
        v_ptr_to_element = new double[v_size];
        // Copy the elements from the other Vector
        for (int i = 0; i < v_size; i++)
        {
            v_ptr_to_element[i] = other[i];
        }
    }
    return *this;
}

// Move operator
Vector& Vector::operator=(Vector&& other)
{
    if (this != &other)
    {
        // Free the memory allocated for this Vector
        delete[] v_ptr_to_element;
        // Copy the size attribute from the other Vector
        v_size = other.v_size;
        // Copy the pointer to the elements from the other Vector
        v_ptr_to_element = other.v_ptr_to_element;
        // Set the other Vector's pointer to nullptr to avoid freeing the same memory twice
        other.v_ptr_to_element = nullptr;
        other.v_size = 0;
    }
    return *this;
}

// Overload of comparison operator
bool Vector::operator==(const Vector& other) const
{
    // set tolerence
    const double tol = 1e-3;

    // Dimension check
    if (v_size != other.getSize())
    {
        return false;
    }

    // Element check
    for (int i = 0; i < v_size; i++)
    {
        if (std::abs(v_ptr_to_element[i] - other[i]) > tol)
        {
            return false;
        }
    }
    return true;
}

// Overload the << operator to print the Vector
std::ostream& operator<<(std::ostream& os, const Vector& vec)
{
    const double threshold = 1e-3;
    for (int i = 0; i < vec.getSize(); i++)
    {
        double value = vec[i];
        if (std::abs(value) < threshold)
        {
            os << std::fixed << std::setprecision(3) << std::abs(value) << " ";
        }
        else
        {
            os << std::fixed << std::setprecision(3) << value << " ";
        }
    }
    os << std::endl;
    return os;
}


// Non member functions

// Dot product between Vectors
double dot_product(const Vector& Vector1, const Vector& Vector2)
{
    double result = 0.0;
    if (Vector1.getSize() != Vector2.getSize())
    {
        std::cout << "Warning! Impossible dot product call. Vector size mismatch." << std::endl;
        exit(1);
    }
    for (int i = 0; i < Vector1.getSize(); i++)
    {
        result += Vector1[i] * Vector2[i];
    }
    return result;
}

// Calculates the product between a Matrix and a Vector
Vector Matrix_Vector_product(const Matrix& mat, const Vector& vec)
{
    int mat_cols = mat.getCols(), mat_rows = mat.getRows(), vec_size = vec.getSize();
    if (vec_size != mat_cols)
    {
        std::cout << "Error, mismatched dimensions for Matrix Vector multiplication. " << std::endl;
        exit(1);
    }
    Vector res_vec(mat_rows);
    for (int i = 0; i < mat_rows; i++)
    {
        double element = 0;
        for (int j = 0; j < mat_cols; j++)
        {
            element += (mat[i][j] * vec[j]);
        }
        res_vec[i] = element;
    }
    return res_vec;
}

// Calculate the VV^T matrix
Matrix outerProduct(const Vector& Vector1, const Vector& Vector2)
{
    const int dim = Vector1.getSize();
    if (dim != Vector2.getSize())
    {
        std::cout << "Error: dimension mismatch for VV^T. " << std::endl;
        exit(1);
    }
    Matrix res(dim, dim);
    for (int i = 0; i < dim; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            res[i][j] = Vector1[i] * Vector2[j];
        }
    }
    return res;
}

// Calculate the V^TV double
double innerProduct(const Vector& Vector1, const Vector& Vector2)
{
    const int dim = Vector1.getSize();
    if (dim != Vector2.getSize())
    {
        std::cout << "Error: dimension mismatch for V^TV. " << std::endl;
        exit(1);
    }
    double res = 0.0;
    for (int i = 0; i < dim; i++)
    {
        res += Vector1[i] * Vector2[i];
    }
    return res;
}
