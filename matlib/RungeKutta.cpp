#include<iostream>
#include<cmath>
#include<functional>
#include<RungeKutta.hpp>
#include<Generic_list.hpp>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>

RungeKutta::RungeKutta(std::function<Vector(double, Vector)> f)
    :   f_(f)
{
}

std::pair<Vector, Vector> RungeKutta::stepper12(const double x, const Vector y, const double h)
{
    Vector k0 = f_(x, y); // embedded lower order formula (Euler)
    Vector k1 = f_(x + h / 2, y + k0 * (h / 2)); // higher order formula (midpoint)
    Vector yh = y + k1 * h; // y(x+h) estimate
    Vector er = (k1 - k0) * h; // error estimate
    return std::make_pair(yh, er);
}

std::pair<Generic_list<double>, Generic_list<Vector>> RungeKutta::driver
(
    double a,                           /* the start-point a */
    Vector ya,                          /* y(a) */
    double b,                           /* the end-point of the integration */
    double h,                           /* initial step-size */
    double acc,                         /* absolute accuracy goal */
    double eps,                         /* relative accuracy goal */
    Generic_list<double>* ptr_x_list,   /* ptr to a x_list */
    Generic_list<Vector>* ptr_y_list    /* ptr to a y_list */
)
{
    // Sanity check on driver step
    if (a > b)
    {
        std::cerr << "ERROR: A>B IN RUNGE KUTTA DRIVER" << std::endl;
        exit(1);
    }

    // Initialise the vectors needed for the while loop
    double x = a;
    Vector y(ya);
    if (ptr_x_list != nullptr && ptr_y_list != nullptr)
    {
        ptr_x_list->add(x);
        ptr_y_list->add(y);
    }
    // Drive
    while (true)
    {
        // End check
        if (x >= b)
        {
            if (ptr_x_list == nullptr || ptr_y_list == nullptr)
            {
                Generic_list<double> x_ans;
                Generic_list<Vector> y_ans;
                x_ans.add(x);
                y_ans.add(y);
                return std::make_pair(x_ans, y_ans);
            }
            else
            {
                return std::make_pair(*ptr_x_list, *ptr_y_list);
            }
        }

        // Making sure we don't overstep final bound
        if (x + h > b)
        {
            h = b - x;
        }

        // Use stepper
        std::pair<Vector, Vector> step_result = RungeKutta::stepper12(x, y, h);
        Vector yh = step_result.first;
        Vector erv = step_result.second;
        Vector tol(y.getSize());
        for (int i = 0; i < y.getSize(); i++)
        {
            tol[i] = (acc + eps * std::abs(yh[i])) * std::sqrt(h / (b - a));
        }

        // Check if step is accepted
        bool ok = true;
        for (int i = 0; i < y.getSize(); i++)
        {
            if (std::abs(erv[i]) >= tol[i])
            {
                ok = false;
                break;
            }
        }
        if (ok)
        {
            x += h;
            y = yh;
            if (ptr_x_list != nullptr && ptr_y_list != nullptr)
            {
                ptr_x_list->add(x);
                ptr_y_list->add(y);
            }
        }

        // Find new step size
        double factor = tol[0] / std::abs(erv[0]);
        for (int i = 1; i < y.getSize(); i++)
        {
            factor = std::min(factor, tol[i] / std::abs(erv[i]));
        }
        h *= std::min(std::pow(factor, 0.25) * 0.95, 2.0);
    }
}
