#include<iostream>
#include<functional>
#include<string>
#include<cmath>
#include<NeuralNetwork.hpp>
#include<Minimization.hpp>
#include<Vector_class.hpp>

// Implementing the constructor
NeuralNetwork::NeuralNetwork(const int n, std::string activationFunctionName)
    :   n (n)
{
    this->parameters = Vector(3 * n);
    if (activationFunctionName == "GaussianWavelet")
    {
        this->activationFunction = [](const double x) {return x * std::exp(-std::pow(x, 2.0));};
    }
    else
    {
        std::cerr << "ERROR: unimplemented activation function requested" << std::endl;
        exit(1);
    }
}

// Implementing the destructor
NeuralNetwork::~NeuralNetwork()
{
}

// Training function
void NeuralNetwork::train(const Vector& xs, const Vector& ys, double step, double accuracy)
{

    // Dimension check
    const int n_data_points = xs.getSize();
    if (n_data_points != ys.getSize())
    {
        std::cerr << "ERROR: mismatched x and y dimensions" << std::endl;
    }
    if (n_data_points < 2)
    {
        std::cerr << "ERROR: at least two data points are required" << std::endl;
    }

    // Initialize parameters
    for (int i = 0; i < 3 * n; i += 3)
    {
        parameters[i] = xs[0] + (xs[n_data_points - 1] - xs[0]) * (i / 3) / (n - 1);
        parameters[i + 1] = 1.0;
        parameters[i + 2] = 1.0;
    }
    
    // Define the cost function
    const std::function<double(const Vector&)> cost_function = [xs, ys, this](const Vector& param)
    {
        // Response reimplemented here for proper capture of parameters
        const std::function<double(const double)> response_opt = [param, this](const double xi)
        {
            double sum = 0.0;
            for (int i = 0; i < param.getSize(); i += 3)
            {
                sum += param[i + 2] * activationFunction((xi - param[i]) / param[i + 1]);
            }
            return sum;
        };
        double sum = 0.0;
        for (int i = 0; i < xs.getSize(); i++)
        {
            sum += std::pow(response_opt(xs[i]) - ys[i], 2.0);
        }
        return sum / (double)xs.getSize();
    };

    // Optimise the cost function
    int n_calls = 0; // Currently unused
    Vector optParam = Minimization::downhillSimplex(cost_function, parameters, n_calls, step, accuracy);
    parameters = optParam;
}

// Function to evaluate response to a given input
double NeuralNetwork::response(const double x) const
{
    double sum = 0.0;
    for (int i = 0; i < 3 * n; i += 3)
    {
        sum += parameters[i + 2] * activationFunction((x - parameters[i]) / parameters[i + 1]);
    }
    return sum;
}

double NeuralNetwork::getDerivative(const double x) const
{
    double sum = 0.0;
    for (int i = 0; i < 3 * n; i += 3)
    {
        double a = parameters[i];
        double b = parameters[i + 1];
        double w = parameters[i + 2];
        double x_transformed = (x - a) / b;
        double prefactor = w * std::exp(-std::pow(x_transformed, 2.0)) / std::pow(b, 3.0);
        double bracket = std::pow(b, 2.0) - 2 * std::pow(a - x, 2.0);
        sum += prefactor * bracket;
    }
    return sum;
}

double NeuralNetwork::getSecondDerivative(const double x) const
{
    double sum = 0.0;
    for (int i = 0; i < 3 * n; i += 3)
    {
        double a = parameters[i];
        double b = parameters[i + 1];
        double w = parameters[i + 2];
        double x_transformed = (x - a) / b;
        double prefactor = 2 * w * (a - x) * std::exp(-std::pow(x_transformed, 2.0)) / std::pow(b, 5.0);
        double bracket = 3 * std::pow(b, 2.0) - 2 * std::pow(a - x, 2.0);
        sum += prefactor * bracket;
    }
    return sum;
}

double NeuralNetwork::getAntiDerivative(const double x) const
{
    double sum = 0.0;
    for (int i = 0; i < 3 * n; i += 3)
    {
        double a = parameters[i];
        double b = parameters[i + 1];
        double w = parameters[i + 2];
        double x_transformed = (x - a) / b;
        sum += -b / 2 * w * std::exp(-std::pow(x_transformed, 2.0));
    }
    return sum;
}