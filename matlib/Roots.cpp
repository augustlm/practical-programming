#include<iostream>
#include<functional>
#include<cmath>
#include<Roots.hpp>
#include<QRGS.hpp>
#include<Vector_class.hpp>
#include<Matrix_class.hpp>


Vector Roots::newtonBacktracking
    (   const std::function<const Vector (const Vector&)>& f        /* Function we want to find roots of */
    ,   const Vector& x                                             /* Starting guess */
    ,   const double epsilon                                        /* Accuracy goal */
    )
{
    // First we need a copy of the initial guess
    const int dim = x.getSize();
    Vector x_guess = x;

    // Convergence criteria on a while loop
    while (f(x_guess).norm() > epsilon)
    {
        // Calculate the jacobian vector and innit R matrix for QR decomposition
        Matrix jacobian = Roots::finiteDifferenceJacobian(f, x_guess);
        Matrix R(dim, dim);

        // Use linear equation solver to find update steps
        QRGS::decomp(jacobian, R);
        Vector delta_x = QRGS::solve(jacobian, R, f(x_guess) * -1);

        // Init step size
        double lambda = 1.0;

        // Simple backtracking while loop to update step size
        while (f(x_guess + delta_x * lambda).norm() > (1 - lambda / 2.0) * f(x_guess).norm() && lambda >= 1.0 / 1024.0)
        {
            lambda = lambda / 2.0;
        }

        // Use lambda to update x_guess
        x_guess = x_guess + delta_x * lambda;
    }

    // Return the solution
    return x_guess;
}


Matrix Roots::finiteDifferenceJacobian
    (   const std::function<const Vector (const Vector&)>& f        /* Function we want to find the jacobian */
    ,   const Vector& x                                             /* Current guess */
    )
{
    // Init a new result vector and a change x vector
    const int dim = x.getSize();
    Matrix jacobian(dim, dim);
    Vector x_changed = x;

    // Evaluate each element of the jacobian result
    for (int k = 0; k < dim; k++)
    {
        // Shift is calculated as |x_i| * sqrt(machine epsilon for double)
        double delta_k = std::abs(x[k]) * std::pow((double)2.0, (double)-26.0);

        // Apply shift to element
        x_changed[k] += delta_k;
        // Evaluate the difference vector
        Vector difference = (f(x_changed) - f(x)) / delta_k;
        // Write elements into the jacobian
        for (int i = 0; i < dim; i++)
        {
            jacobian[i][k] = difference[i];
        }

        // Undo the shift
        x_changed[k] = x[k];
    }
    return jacobian;
}
