#include<iostream>
#include<cmath>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<EVD.hpp>

// Constuctor (No defaults or anything)
EVD::EVD(Matrix& M, const bool opt, const double precision) 
{
    int size = M.getCols();
    // Check M is square
    if (size != M.getRows())
    {
        std::cout << "ERROR: CALLING EVD ON NON-SQUARE MATRIX" << std::endl;
        exit(1);
    }
    
    // Initialize w and V full of zeros
    V = Matrix::identity(size);
    w = Vector(size);
    

    // Run Jacobi rotations on M and update V and w.
    // Optimised vesion preserves M saving memory
    if (opt == true)
    {
        for (int i = 0; i < size; i++)
        {
            w[i] = M[i][i];
        }
        cyclicOpt(M, V, w, precision);
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < i; j++)
            {
                M[j][i] = M[i][j];
            }
        }
    }
    // Unoptimised version requires copy to break
    else
    {
        Matrix A = M;
        cyclic(A, V, w, precision);

        // Update w with diagonal of A
        for (int i = 0; i < size; i++)
        {
            w[i] = A[i][i];
        }
    }
}

// Destructor
EVD::~EVD() 
{
}

// A couple of getters to access the stuff we made private
Vector EVD::getEigenvalues() const 
{
    return w;
}

Matrix EVD::getEigenvectors() const 
{
    return V;
}

// Now the private timesJ and Jtimes functions
void EVD::timesJ(Matrix& A, int p, int q, double theta)
{
    double cos_theta = std::cos(theta);
    double sin_theta = std::sin(theta);
    for (int i = 0; i < A.getCols(); i++)
    {
        // Get the values
        double A_ip = A[i][p];
        double A_iq = A[i][q]; // This may be the wrong way around?

        // Update the values
        A[i][p] = cos_theta * A_ip - sin_theta * A_iq;
        A[i][q] = sin_theta * A_ip + cos_theta * A_iq;
    }
}

void EVD::Jtimes(Matrix& A, int p, int q, double theta)
{
    double cos_theta = std::cos(theta);
    double sin_theta = std::sin(theta);
    for (int j = 0; j < A.getCols(); j++)
    {
        // Get the values
        double A_pj = A[p][j];
        double A_qj = A[q][j]; // This may be the wrong way around?

        // Update the values
        A[p][j] =  cos_theta * A_pj + sin_theta * A_qj;
        A[q][j] = -sin_theta * A_pj + cos_theta * A_qj;
    }
}

// Unoptimised cyclic operations
void EVD::cyclic(Matrix& A, Matrix& V, Vector& w, const double precision)
{
    const int size = A.getCols();
    bool changed = true;
    while (changed)
    {
        changed = false;
        for (int p = 0; p < size - 1; p++)
        {
            for (int q = p + 1; q < size; q++)
            {
                double A_pq = A[p][q], A_pp = A[p][p], A_qq = A[q][q];
		        double theta = 0.5 * std::atan2(2 * A_pq, A_qq - A_pp);
                double cos_theta = std::cos(theta), sin_theta = std::sin(theta);
                double new_A_pp = cos_theta * cos_theta * A_pp - 2 * sin_theta * cos_theta * A_pq + sin_theta * sin_theta * A_qq;
                double new_A_qq = sin_theta * sin_theta * A_pp + 2 * sin_theta * cos_theta * A_pq + cos_theta * cos_theta * A_qq;
                if(std::abs(new_A_pp - A_pp) > precision || std::abs(new_A_qq - A_qq) > precision) // do rotation
                {
                    changed = true;
                    timesJ(A, p, q, theta);
                    Jtimes(A, p, q, -theta);
                    timesJ(V, p, q, theta);
                }
            }
        }
    }
}

// Perform the cyclic operations in an optimised manner
void EVD::cyclicOpt(Matrix& A, Matrix& V, Vector& w, const double precision)
{
    const int size = A.getCols();
    bool changed = true;
    while (changed)
    {
        changed = false;
        for (int p = 0; p < size; p++)
        {
            for (int q = p + 1; q < size; q++)
            {
                double A_pq = A[p][q], A_pp = w[p], A_qq = w[q];
		        double theta = 0.5 * std::atan2(2 * A_pq, A_qq - A_pp);
                double cos_theta = std::cos(theta), sin_theta = std::sin(theta);
                double new_A_pp = cos_theta * cos_theta * A_pp - 2 * sin_theta * cos_theta * A_pq + sin_theta * sin_theta * A_qq;
                double new_A_qq = sin_theta * sin_theta * A_pp + 2 * sin_theta * cos_theta * A_pq + cos_theta * cos_theta * A_qq;
                if(std::abs(new_A_pp - A_pp) > precision || std::abs(new_A_qq - A_qq) > precision) // do rotation
                {
                    changed = true;

                    // Optimised routine presevers the diagonal
                    w[p] = new_A_pp;
                    w[q] = new_A_qq;
                    A[p][q] = 0.0;

                    for (int i = 0; i < p; i++)
                    {
                        // Get the values
                        double A_ip = A[i][p];
                        double A_iq = A[i][q];

                        // Update the values
                        A[i][p] = cos_theta * A_ip - sin_theta * A_iq;
                        A[i][q] = sin_theta * A_ip + cos_theta * A_iq;
                    }
                    for (int i = p+1; i < q; i++)
                    {
                        // Get the values
                        double A_ip = A[p][i];
                        double A_iq = A[i][q];

                        // Update the values
                        A[p][i] = cos_theta * A_ip - sin_theta * A_iq;
                        A[i][q] = sin_theta * A_ip + cos_theta * A_iq;
                    }
                    for (int i = q+1; i < size; i++)
                    {
                        // Get the values
                        double A_ip = A[p][i];
                        double A_iq = A[q][i];

                        // Update the values
                        A[p][i] = cos_theta * A_ip - sin_theta * A_iq;
                        A[q][i] = sin_theta * A_ip + cos_theta * A_iq;
                    }
                    for (int i = 0; i < size; i++)
                    {   
                        // Get the values
                        double V_ip = V[i][p];
                        double V_iq = V[i][q];

                        // Update the values
                        V[i][p]=cos_theta * V_ip - sin_theta * V_iq;
                        V[i][q]=cos_theta * V_iq + sin_theta * V_ip;
                    }
                } // Change block
            } // loop over q
        } // loop over p
    } // while changed
}
