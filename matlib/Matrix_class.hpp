#pragma once

class Vector;

class Matrix
{
private:

    // Define the number of rows and cols in Matrix as well as pointer to pointer to element.
    int m_num_cols;
    int m_num_rows;
    double** m_ptr_to_element;

public:

    // Defining constructors
    Matrix();
    Matrix(const int num_rows, const int num_cols);
    Matrix(const Matrix& other);

    // Defining destructors
    ~Matrix();

    // Public member functions
    int getRows() const;
    int getCols() const;
    void scale(const double factor);
    static Matrix identity(const int size);
    static Matrix randomMatrixInt(const int height, const int width, const int shift=0, const int max=RAND_MAX);

    // Operators
    Matrix operator+(const Matrix& other) const;
    double* operator[](const int i);
    const double* operator[](const int i) const;
    Matrix& operator=(const Matrix& other);
    Matrix& operator=(Matrix&& other);
    bool operator==(const Matrix& other) const;
    friend std::ostream& operator<<(std::ostream& os, const Matrix& mat);

};

// Non member functions
Matrix Matrix_product(const Matrix& mat_1, const Matrix& mat_2);
Matrix Vector_Matrix_product(const Vector& vec, const Matrix& mat);
Matrix transpose(const Matrix& mat);
