#pragma once

#include <Vector_class.hpp>

class Qspline
{
private:
    Vector x;
    Vector y;
    const int n;
    Vector b;
    Vector c;

public:

    // Constructors
    Qspline(const Vector& xs, const Vector& ys);

    // Destructors
    ~Qspline();

    // member functions
    double evaluate(const double z) const;
    Vector getB() const;
    Vector getC() const;

    // static member functions
    static double linearSplineIntegral(const Vector& xs, const Vector& ys, const double z);
    static double linearInterpolation(const Vector& xs, const Vector& ys, const double z);
    static int binomialSearch(const Vector& xs, const double z);

};

