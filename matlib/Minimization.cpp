#include<iostream>
#include<functional>
#include<cmath>
#include<Minimization.hpp>
#include<Vector_class.hpp>
#include<Matrix_class.hpp>
#include<Generic_list.hpp>
#include<QRGS.hpp>

Vector Minimization::quasiNewton
(
    const std::function<double(const Vector&)>& f,         /* Function to minimize */
    const Vector& starting_point,                          /* Initial guess */
    int& iter_cout,                                        /* Number of iterations */
    const double accuracy                                  /* The accuracy aim */
)
{
    // Initialize some variables
    Vector x(starting_point);
    const int dimensions = x.getSize();
    Matrix B = Matrix::identity(dimensions);
    Vector gradient = Minimization::finiteDifferenceGradient(f, x);
    double alpha = 0.0001;
    double epsilon = 0.000001;
    double lambda_min = 0.00001;

    // Loop till converged
    while (gradient.norm() > accuracy)
    {
        // Increment counter
        iter_cout++;

        // Calculating newton step
        Vector delta_x = Matrix_Vector_product(B, gradient) * -1.0;

        // Do linsearch
        double lambda = 1.0;
        while (f(x + delta_x * lambda) > f(x) + alpha * innerProduct(delta_x * lambda, gradient) && lambda >= lambda_min)
        {
            lambda = lambda / 2.0;
        }

        // Update Hessian based on the result
        if (lambda < lambda_min)
        {
            B = Matrix::identity(dimensions);
        }
        else
        {
            // Find s, y and u vectors
            Vector s = delta_x * lambda;
            Vector y = Minimization::finiteDifferenceGradient(f, x + s) - gradient;
            Vector u = s - Matrix_Vector_product(B, y);

            // SR1 update for delta B
            Matrix delta_B = outerProduct(u, u);
            double inner_product = innerProduct(u, y);

            // Denominator check before updating B
            if (inner_product > epsilon)
            {
                delta_B.scale(1.0 / inner_product);
                B = B + delta_B;
            }
        }
        
        // Update x and gradient
        x = x + delta_x * lambda;
        gradient = Minimization::finiteDifferenceGradient(f, x);
    }

    // Return the minimum point
    return x;
}

Vector Minimization::finiteDifferenceGradient
(
    const std::function<double(const Vector&)>& f,          /* Function to evaluate gradient of */
    const Vector& x                                         /* Point at which to evaluate gradient */
)
{
    // Initialize needed vectors
    Vector x_changed(x);
    Vector gradient(x.getSize());

    // Loop over dimensions
    for (int i = 0; i < x.getSize(); i++)
    {
        // Shift is calculated as |x_i| * sqrt(machine epsilon for double)
        double delta_x_i = std::abs(x[i]) * std::pow((double)2.0, (double)-26.0);
        x_changed[i] += delta_x_i;

        // Calculate gradient using shift
        gradient[i] = -(f(x) - f(x_changed)) / delta_x_i;

        // Undo the shift
        x_changed[i] = x[i];
    }

    return gradient;
}

Vector Minimization::downhillSimplex
(
    const std::function<double(const Vector&)>& f,          /* Function to minimize*/
    const Vector& x_start,                                  /* Initial simplex to optimise */
    int& iter_count,                                        /* Number of iterations */
    const double step,                                      /* Deviation for start simplex */
    const double accuracy                                   /* The accuracy aim */
)
{
    // Convergence check lambda
    const std::function<double(const Generic_list<Vector> x_list)> simplexConvergence = [f](const Generic_list<Vector> x_list)
    {
        // Convergence check using size
        double s = 0;
        for (int i = 1; i < x_list.get_size(); i++) 
        {
            double norm = (x_list[0] - x_list[i]).norm();
            s = std::max(s, norm);
        }
        return s;
    };

    // Initialisation of needed variables
    const int dim = x_start.getSize();
    const int simplex_size = dim + 1;
    Vector x_copy(x_start);
    int max_idx;
    int min_idx;

    // Make the simplex
    Generic_list<Vector> simplex;
    for (int i = 0; i < dim; i++)
    {
        x_copy[i] += step;
        simplex.add(x_copy);
        x_copy = x_start;
    }
    simplex.add(x_copy);

    // Main loop
    while (simplexConvergence(simplex) > accuracy)
    {
        iter_count++;

        // Find min and max points
        max_idx = 0;
        min_idx = 0;
        double max = f(simplex[0]);
        double min = f(simplex[0]);
        for (int i = 1; i < simplex_size; i++)
        {
            double test_value = f(simplex[i]);
            if (test_value > max)
            {
                max_idx = i;
                max = test_value;
            }
            if (test_value < min)
            {
                min_idx = i;
                min = test_value;
            }
        }

        // Find the centroid point
        Vector centroid(dim);
        for (int i = 0; i < simplex_size; i++)
        {
            if (i == max_idx)
            {
                continue;
            }
            centroid = centroid + simplex[i];
        }
        centroid = centroid / (double)dim;

        // Try expansion
        Vector expansion = centroid * 3.0 - simplex[max_idx] * 2.0;
        double f_expansion = f(expansion);
        if (f_expansion < (f(simplex[min_idx])))
        {
            simplex[max_idx] = expansion;
            continue;
        }
        // Try reflection
        Vector reflection = centroid * 2.0 - simplex[max_idx];
        double f_reflection = f(reflection);
        if (f_reflection < (f(simplex[max_idx])))
        {
            simplex[max_idx] = reflection;
            continue;
        }
        // Try contraction
        Vector contraction = (centroid + simplex[max_idx]) / 2.0;
        double f_contraction = f(contraction);
        if (f_contraction < f(simplex[max_idx]))
        {
            simplex[max_idx] = contraction;
            continue;
        }
        // Do reduction
        for (int i = 0; i < simplex_size; i++)
        {
            if (i != min_idx)
            {
                simplex[i] = (simplex[i] + simplex[min_idx]) / 2.0;
            }
        }
    }
    return simplex[min_idx];
}
