#pragma once

class Matrix;

class Vector;

class EVD
{
private:

    // The EVD class has a Matrix V (eigenVectors) and w (eigenvalues)
    Matrix V;
    Vector w;

    // Some methods which should never be called directly
    static void timesJ(Matrix& A, int p, int q, double theta);
    static void Jtimes(Matrix& A, int p, int q, double theta);
    static void cyclic(Matrix& A, Matrix& V, Vector& w, const double precision);
    static void cyclicOpt(Matrix& A, Matrix& V, Vector& w, const double precision);

public:

    // Constructors (Don't change the matrix M)
    EVD(Matrix& M, const bool opt=true, const double precision=1e-10);

    // Destructor
    ~EVD();

    // Getters
    Vector getEigenvalues() const;
    Matrix getEigenvectors() const;

};
