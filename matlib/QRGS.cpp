#include<iostream>
#include<cmath>
#include<functional>
#include<utility>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<QRGS.hpp>

void QRGS::decomp(Matrix& a, Matrix& r)
{
    // Defining ranges
    int rows_a = a.getRows();
    int cols_a = a.getCols();

    // loop over cols and init norm
    for (int col_idx = 0; col_idx < cols_a; col_idx++)
    {
        double norm_squared = 0.0;

        // loop over rows and sum norm squared
        for (int row_idx = 0; row_idx < rows_a; row_idx++)
        {
            norm_squared += (a[row_idx][col_idx] * a[row_idx][col_idx]);
        }
        
        // take sqrt of norm squared and put it on the diagonal of R
        double norm = std::sqrt(norm_squared);
        r[col_idx][col_idx] = norm;

        // divide every element in a column by the norm of that column
        for (int row_idx = 0; row_idx < rows_a; row_idx++)
        {
            a[row_idx][col_idx] /= norm;
        }
        
        // take dot product with all other cols
        for (int other_col = col_idx + 1; other_col < cols_a; other_col++)
        {
            double dot_product = 0.0;
            for (int row_idx = 0; row_idx < rows_a; row_idx++)
            {
                dot_product += a[row_idx][col_idx] * a[row_idx][other_col];
            }
            
            // use dot product in r Matrix
            r[col_idx][other_col] = dot_product;

            // update target Matrix using dot product
            for (int row_idx = 0; row_idx < rows_a; row_idx++)
            {
                a[row_idx][other_col] -= (a[row_idx][col_idx] * dot_product);
            }
        }
    }
}

Vector QRGS::solve(const Matrix& Q, const Matrix& R, const Vector& b)
{
    // Find the transpose of Q
    const Matrix Q_trans = transpose(Q);

    // Apply this to b to get new x. This leads to R*ans=x
    Vector x = Matrix_Vector_product(Q_trans, b);

    // Attempt in place back substitution with x and R
    // Start for loop that takes last element of Vector first
    for (int vec_idx = x.getSize() - 1; vec_idx >= 0; vec_idx--)
    {
        // For each vec_ele we need sum of U_vec_ele * x_col
        double sum = 0.0;
        for (int col_idx = vec_idx + 1;  col_idx < x.getSize(); col_idx++)
        {
            sum += R[vec_idx][col_idx] * x[col_idx];
        }

        // Now we update element
        x[vec_idx] = (x[vec_idx] - sum) / R[vec_idx][vec_idx];
    }
    return x;
}

double QRGS::det(const Matrix& R)
{
    // init a counter
    double result = R[0][0];

    // minimal idiot proofing (note I am not checking for triangle)
    if (R.getCols() != R.getRows())
    {
        std::cout << "ERROR: CALLING DET ON NON-SQUARE Matrix" << std::endl;
        exit(1);
    }
    
    // run along diagonal and sum
    for (int idx = 1; idx < R.getCols(); idx++)
    {
        result *= R[idx][idx];
    }
    return result;
}

Matrix QRGS::inverse(const Matrix& Q, const Matrix& R)
{
    // idiot proofing
    if (Q.getCols() != Q.getRows())
    {
        std::cout << "ERROR: INVERTING NON-SQUARE Matrix" << std::endl;
        exit(1);
    }
    
    // init the return Matrix
    Matrix result(Q.getRows(), Q.getCols());

    // for each col we need a Vector containing 1 at col position
    for (int col_idx = 0; col_idx < result.getCols(); col_idx++)
    {
        Vector unit(result.getRows());
        unit[col_idx] = 1;

        // now solve QRx = unit
        Vector col_res = QRGS::solve(Q, R, unit);

        // put the elements of col_res into the appropriate places
        for (int row_idx = 0; row_idx < result.getRows(); row_idx++)
        {
            result[row_idx][col_idx] = col_res[row_idx];
        }
    }
    return result;
}

const std::pair<Vector, Matrix> QRGS::lsfit(const std::function<double(double)> fs[], const int num_func, const Vector& x, const Vector& y, const Vector& dy)
{
    // Set the dimensions n and m
    const int n = x.getSize();
    const int m = num_func;

    // Creating A and b for solving the Least Squares
    Matrix A(n, m);
    Vector b(n);

    // Looping through all elements in A and b
    for (int i = 0; i < n; i++)
    {
        b[i] = y[i] / dy[i];
        for (int j = 0; j < m; j++)
        {
            A[i][j] = fs[j](x[i]) / dy[i];
        }
    }

    // Creating R matrix
    Matrix R(m, m);
    
    // Running Decomp and Solve
    QRGS::decomp(A, R); // Destroys A
    Vector c = QRGS::solve(A, R, b);

    // Invert matrix and find S
    Matrix dummy_R(m, m);
    QRGS::decomp(R, dummy_R);
    Matrix R_inv = QRGS::inverse(R, dummy_R); // Destroys R
    Matrix S = Matrix_product(R_inv, transpose(R_inv));

    // Return value
    std::pair<Vector, Matrix> return_values(c, S);
    return return_values;
}
