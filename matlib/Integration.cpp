#include<iostream>
#include<functional>
#include<limits>
#include<cmath>
#include<random>
#include<ctime>
#include<utility>
#include<Integration.hpp>
#include<Vector_class.hpp>

std::pair<double, double> Integration::recursiveAdaptiveIntegrator
    (   std::function<double(double)> const f           /* The function we will integrate */
    ,   double const a                                  /* The start point of integration */
    ,   double const b                                  /* The end point of integration */
    ,   int& n                                          /* The number of iterations */
    ,   double const delta                              /* Absolute error tolerance */
    ,   double const epsilon                            /* Relative error tolerance */
    ,   double f2                                       /* Evaluation of f at a + 2h / 6 */
    ,   double f3                                       /* Evaluation of f at a + 4h / 6 */
    )
{
    double h = b - a; // Define the area of integration

    // Define the 4 points we will use
    double f1 = f(a + h / 6);
    n++;
    if (std::isnan(f2)) // First iter no points to reuse
    {
        f2 = f(a + 2 * h / 6); 
        n++;
        f3 = f(a + 4 * h / 6);
        n++;
    }
    double f4 = f(a + 5 * h / 6);
    n++;
    
    // Calculate an estimate using a higher order method Q and lower order method q
    double Q = (2 * f1 + f2 + f3 + 2 * f4) / 6 * (b - a);
    double q = (f1 + f2 + f3 + f4) / 4 * (b - a);

    // We then use the difference between the estimates as an error value
    double error = std::abs(Q - q);

    // Stop condition for recursion is based on error
    if (error <= delta + epsilon * std::abs(Q))
    {
        std::pair<double, double> return_value (Q, error);
        return return_value;
    }
    
    // Otherwise we recursively call on each half
    double mid = (a + b) / 2;
    double new_delta = delta / std::sqrt(2);
    std::pair<double, double> left = Integration::recursiveAdaptiveIntegrator(f, a, mid, n, new_delta, epsilon, f1, f2);
    std::pair<double, double> right = Integration::recursiveAdaptiveIntegrator(f, mid, b, n, new_delta, epsilon, f3, f4);
    double value = left.first + right.first;
    double combined_error = left.second + right.second;
    std::pair<double, double> return_value (value, combined_error);
    return return_value;
}

std::pair<double, double> Integration::clenshawCurtisTransformIntegrator
    (   std::function<double(double)> const f                    /* The function we will integrate */
    ,   double const a                                           /* The start point of integration */
    ,   double const b                                           /* The end point of integration */
    ,   int& n                                                   /* The number of iterations */
    ,   double const delta                                       /* Absolute error tolerance */
    ,   double const epsilon                                     /* Relative error tolerance */
    )
{
    std::function<double(double)> inf_transform;
    double trans_a, trans_b;
    if (std::isinf(a) && std::isinf(b))
    {
        inf_transform = [f] (double t) {
            return f(t / (1 - std::pow(t, 2))) * (1 + std::pow(t, 2)) / std::pow(1 - std::pow(t, 2), 2);};
        trans_a = -1.0;
        trans_b = 1.0;
    }
    else if (std::isinf(b))
    {
        inf_transform = [f, a] (double t) {
            return f(a + t / (1 - t)) * 1 / std::pow(1 - t, 2);};
        trans_a = 0;
        trans_b = 1.0;
    }
    else if (std::isinf(a))
    {
        inf_transform = [f, b] (double t) {
            return f(b + t / (1 + t)) * 1 / std::pow(1 + t, 2);};
        trans_a = -1.0;
        trans_b = 0;
    }
    else
    {
        inf_transform = f;
        trans_a = a;
        trans_b = b;
    }
    std::function<double(double)> transformed_func = [trans_a, trans_b, inf_transform](double theta) {
        return inf_transform((trans_a + trans_b) / 2 + (trans_b - trans_a) / 2 * std::cos(theta)) * std::sin(theta) * (trans_b - trans_a) / 2;};
    double new_a = 0;
    double new_b = M_PI;
    return Integration::recursiveAdaptiveIntegrator(transformed_func, new_a, new_b, n, delta, epsilon);
}

std::pair<double, double> Integration::plainMonteCarloIntegrator
    (   std::function<double(Vector)> const f                    /* The function we will integrate */
    ,   Vector const& a                                          /* The vector of start bounds */
    ,   Vector const& b                                          /* The vector of end bounds */
    ,   int const N                                              /* The number of points we will sample */
    )
{
    int const dim = a.getSize();

    // Check the dimension size of a and b
    if (dim != b.getSize())
    {
        std::cerr << "ERROR: dimension size mismatch in plain monte carlo" << std::endl;
        exit(1);
    }
    
    // Find integration volume
    double volume = 1.0;
    for (int i = 0; i < dim; i++)
    {
        volume *= b[i] - a[i];
    }
    
    // Seed random with time
    std::srand(std::time(nullptr));

    // Find the sum and the sum of the squares
    double sum = 0;
    double sum_of_squares = 0;
    Vector x(dim);

    // Loop over points
    for (int i = 0; i < N; i++)
    {
        // Loop over dimensions
        for (int j = 0; j < dim; j++)
        {
            double random_double = (double)std::rand() / (double)RAND_MAX;
            x[j] = a[j] + random_double * (b[j] - a[j]);
        }
        double fx = f(x);
        sum += fx;
        sum_of_squares += std::pow(fx, 2);
    }

    // Find return values
    double mean = sum / (double)N;
    double sigma = std::sqrt(std::abs(sum_of_squares / N - mean * mean));
    double estimate = mean * volume;
    double error = sigma * volume / std::sqrt(N);
    std::pair<double, double> result = std::make_pair(estimate, error);
    return result;
}

std::pair<double, double> Integration::haltonLatticeMonteCarloIntegrator
        (   std::function<double(Vector)> const f                    /* The function we will integrate */
        ,   Vector const& a                                          /* The vector of start bounds */
        ,   Vector const& b                                          /* The vector of end bounds */
        ,   int const N                                              /* The number of points we will sample */
        )
{
    int const dim = a.getSize();

    // Check the dimension size of a and b
    if (dim != b.getSize())
    {
        std::cerr << "ERROR: dimension size mismatch in Halton Lattice monte carlo" << std::endl;
        exit(1);
    }
    
    // Find integration volume
    double volume = 1.0;
    Vector ranges(dim);
    for (int i = 0; i < dim; i++)
    {
        ranges[i] = b[i] - a[i];
    }
    for (int i = 0; i < dim; i++)
    {
        volume *= ranges[i];
    }
    
    // Find the sum and the sum of the squares
    double sum_halton = 0;
    double sum_lattice = 0;

    // Loop over points
    for (int i = 0; i < N; i++)
    {
        Vector rand_h = Integration::haltonRandomNumberGenerator(i, dim);
        Vector rand_l = Integration::latticeRandomNumberGenerator(i, dim);
        for (int j = 0; i < dim; i++)
        {
            rand_h[j] *= ranges[j];
            rand_l[j] *= ranges[j];
            if (rand_h[j] > 1 || rand_l[j] > 1)
            {
                std::cerr << "ERROR: COORDINATE SCALING FAILED IN HALTON LATTCE" << std::endl;
                exit(1);
            }
        }
        Vector x_halton = a + rand_h;
        Vector x_lattice = a + rand_l;
        double f_halton = f(x_halton);
        double f_lattice = f(x_lattice);
        sum_halton += f_halton;
        sum_lattice += f_lattice;
    }

    // Find return values
    double mean_halton = sum_halton / (double)N;
    double mean_lattice = sum_lattice / (double)N;
    double estimate_halton = mean_halton * volume;
    double estimate_lattice = mean_lattice * volume;
    double error = std::abs(estimate_lattice - estimate_halton);
    double estimate = (estimate_halton + estimate_lattice) / 2.0;
    std::pair<double, double> result = std::make_pair(estimate, error);
    return result;
}

double Integration::errorFunction(const double z)
{
    double prefactor = 2.0 / std::sqrt(M_PI);
    int n = 0;
    if (z < 0)
    {
        return -errorFunction(-z);
    }
    else if (z >= 0 && z <= 1)
    {
        std::function<double(double)> f = [](double x) {return std::exp(-(std::pow(x, 2)));};
        return prefactor * Integration::recursiveAdaptiveIntegrator(f, 0.0, z, n).first;
    }
    else
    {
        std::function<double(double)> f = [z](double x) {return std::exp(-(std::pow(z + (1.0 - x) / x, 2.0))) / std::pow(x, 2.0);};
        return 1 - prefactor * Integration::recursiveAdaptiveIntegrator(f, 0.0, 1.0, n).first;
    }
}

double Integration::corputRandomNumberGenerator(int n, int b)
{
    double q = 0;
    double bk = (double)1/b;
    while (n > 0)
    {
        q += (n % b) * bk ;
        n /= b ; 
        bk /= b ;
    }
    return q;
}

Vector Integration::haltonRandomNumberGenerator(int n, int dim)
{
    Vector result(dim);
    const int base[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61};
    const int max_dim = sizeof(base)/sizeof(int);
    if (dim > max_dim)
    {
        std::cerr << "ERROR: too many dimensions in Halton random number generator" << std::endl;
        exit(1);
    }
    for (int i = 0; i < dim; i++)
    {
        result[i] = Integration::corputRandomNumberGenerator(n, base[i]);
    }
    return result;
}

Vector Integration::latticeRandomNumberGenerator(int n, int dim)
{
    long double integral_part;
    long double* iptr = &integral_part; 
    Vector alpha(dim);
    for (int i = 0; i < dim; i++)
    {
        alpha[i] = std::modf(n * std::sqrt(M_PIl + i), iptr);
    }
    return alpha;
}

std::pair<double, double> Integration::twoDIntegration
    (   const std::function<double(double, double)> f            /* The function we will integrate */
    ,   const double a                                           /* Start of outter interval */
    ,   const double b                                           /* End of outer interval */
    ,   const std::function<double(double)> d                    /* Inner interval */
    ,   const std::function<double(double)> u                    /* Outer interval */
    ,   const double eps                                         /* Epsilon */
    ,   const double acc                                         /* Accuracy */
    ,   int& n                                                   /* Number of iterations called */
    ,   const bool clenshaw_curtis_transform                     /* Toggle use of clenshaw transformation */
    )
{
    std::function<double(double)> inner_integral = [f, d, u, eps, acc, &n, clenshaw_curtis_transform](double x) {
        std::function<double(double)> inner_function = [f, x, eps, acc, &n](double y){
            return f(x, y);
        };
        if (clenshaw_curtis_transform == true)
        {
            return Integration::clenshawCurtisTransformIntegrator(inner_function, d(x), u(x), n, acc, eps).first;
        }
        else
        {
            return Integration::recursiveAdaptiveIntegrator(inner_function, d(x), u(x), n, acc, eps).first;
        }
    };
    if (clenshaw_curtis_transform == true)
    {
        return Integration::clenshawCurtisTransformIntegrator(inner_integral, a, b, n, acc, eps);
    }
    else
    {
        return Integration::recursiveAdaptiveIntegrator(inner_integral, a, b, n, acc, eps);
    }
}
