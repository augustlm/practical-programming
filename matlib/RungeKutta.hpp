#pragma once

#include<functional>
#include<utility>

template <typename L> class Generic_list;

class Vector;

class RungeKutta
{
private:

    // The function f from dy/dx = f(x,y)
    std::function<Vector(double, Vector)> f_;

    // Steppers. A only requires 12 for part A
    std::pair<Vector, Vector> stepper12(const double x, const Vector y, const double h);

public:

    // Constructor takes the function f from dy/dx = f(x,y)
    RungeKutta(std::function<Vector(double, Vector)> f);

    // Driver
    std::pair<Generic_list<double>, Generic_list<Vector>> driver
    (
        double a,                                   /* the start-point a */
        Vector ya,                                  /* y(a) */
        double b,                                   /* the end-point of the integration */
        double h,                                   /* initial step-size */
        double acc,                                 /* absolute accuracy goal */
        double eps,                                 /* relative accuracy goal */
        Generic_list<double>* ptr_x_list=nullptr,   /* ptr to a x_list */
        Generic_list<Vector>* ptr_y_list=nullptr    /* ptr to a y_list */
    );
};