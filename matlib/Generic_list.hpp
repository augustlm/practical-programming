#pragma once

#include<stdexcept>

template <typename T> class Generic_list
{
private:
    
    // Data values
    int l_size;
    T* ptr_to_data;

public:

    // Constructors
    Generic_list();
    Generic_list(int size);
    Generic_list(const Generic_list& other);
    Generic_list(Generic_list&& other);

    // Destructor
    ~Generic_list();

    // Public member functions
    const int get_size() const;
    void add(T item);

    // Operator
    T& operator[](const int i);
    const T& operator[](const int i) const;
    Generic_list& operator=(const Generic_list& other);
    Generic_list& operator=(Generic_list&& other);
};


// Implementations

// Constructors

// Default
template <typename T> Generic_list<T>::Generic_list()
    :   l_size (0)
    ,   ptr_to_data (nullptr)
{
}

// Standard
template <typename T> Generic_list<T>::Generic_list(int size)
    :   l_size (size)
{
    ptr_to_data = new T[size];
}

// Copy
template <typename T> Generic_list<T>::Generic_list(const Generic_list& other)
    :   l_size (other.l_size)
{
    ptr_to_data = new T[other.l_size];
    for (int i = 0; i < other.l_size; i++)
    {
        ptr_to_data[i] = other[i];
    }   
}

// Move
template <typename T>
Generic_list<T>::Generic_list(Generic_list&& other)
    : l_size(other.l_size)
    , ptr_to_data(other.ptr_to_data)
{
    other.l_size = 0;
    other.ptr_to_data = nullptr;
}

// Destructors

// Standard
template <typename T> Generic_list<T>::~Generic_list()
{
    if (ptr_to_data != nullptr)
    {
        delete[] ptr_to_data;
        ptr_to_data = nullptr;
    }
    
}

// Public member functions

// Get_size returns the size of the list
template <typename T>
const int Generic_list<T>::get_size() const
{
    return this->l_size;
}

// Add
template <typename T>
void Generic_list<T>::add(T item)
{
    T* new_ptr_to_data = new T[l_size + 1];
    for (int i = 0; i < l_size; i++)
    {
        new_ptr_to_data[i] = ptr_to_data[i];
    }
    new_ptr_to_data[l_size] = item;
    delete[] ptr_to_data;
    ptr_to_data = new_ptr_to_data;
    l_size += 1;
}


// Operators

// Access operator [] non const
template <typename T>
T& Generic_list<T>::operator[](const int i)
{
    if (i<0 || i >= l_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return ptr_to_data[i];
}

// Access operator [] const
template <typename T>
const T& Generic_list<T>::operator[](const int i) const
{
    if (i<0 || i >= l_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return ptr_to_data[i];
}

// Copy assignment operator
template <typename T>
Generic_list<T>& Generic_list<T>::operator=(const Generic_list& other)
{
    if (this != &other) // avoid self-assignment
    {
        // deallocate old resources
        delete[] ptr_to_data;
        l_size = 0;
        // allocate new resources
        l_size = other.l_size;
        ptr_to_data = new T[l_size];
        for (int i = 0; i < l_size; i++)
        {
            ptr_to_data[i] = other.ptr_to_data[i];
        }
    }
    return *this;
}

// Move assignment operator
template <typename T>
Generic_list<T>& Generic_list<T>::operator=(Generic_list&& other)
{
    if (this != &other) // avoid self-assignment
    {
        // deallocate old resources
        delete[] ptr_to_data;
        // move new resources
        l_size = other.l_size;
        ptr_to_data = other.ptr_to_data;
        // clear moved object
        other.l_size = 0;
        other.ptr_to_data = nullptr;
    }
    return *this;
}
