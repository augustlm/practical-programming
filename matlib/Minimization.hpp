#pragma once

#include<functional>

template <typename L> class Generic_list;

class Vector;

class Minimization
{
public:
    static Vector quasiNewton
    (
        const std::function<double(const Vector&)>& f,          /* Function to minimize */
        const Vector& starting_point,                           /* Initial guess */
        int& iter_cout,                                         /* Number of iterations */
        const double accuracy = 0.0001                          /* The accuracy aim */
    );

    static Vector finiteDifferenceGradient
    (
        const std::function<double(const Vector&)>& f,          /* Function to evaluate gradient of */
        const Vector& x                                         /* Point at which to evaluate gradient */
    );

    static Vector downhillSimplex
    (
        const std::function<double(const Vector&)>& f,          /* Function to minimize*/
        const Vector& x_start,                                  /* Initial simplex to optimise */
        int& iter_count,                                        /* Number of iterations */
        const double step = 1.0/64.0,                           /* Deviation for start simplex */
        const double accuracy = 0.0001                          /* The accuracy aim */
    );
};