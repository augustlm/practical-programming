#pragma once

#include<functional>
#include<limits>

class Vector;

class Integration
{
public:
    // Recursive Adaptive Integration Routine
    static std::pair<double, double> recursiveAdaptiveIntegrator
        (   std::function<double(double)> const f                    /* The function we will integrate */
        ,   double const a                                           /* The start point of integration */
        ,   double const b                                           /* The end point of integration */
        ,   int& n                                                   /* The number of iterations */
        ,   double const delta = 0.001                               /* Absolute error tolerance */
        ,   double const epsilon = 0.001                             /* Relative error tolerance */
        ,   double f2 = std::numeric_limits<double>::quiet_NaN()     /* Evaluation of f at a + 2h / 6 */
        ,   double f3 = std::numeric_limits<double>::quiet_NaN()     /* Evaluation of f at a + 4h / 6 */
        );

    // Clenshaw Curtis Transformation of Integration Routine
    static std::pair<double, double> clenshawCurtisTransformIntegrator
        (   std::function<double(double)> const f                    /* The function we will integrate */
        ,   double const a                                           /* The start point of integration */
        ,   double const b                                           /* The end point of integration */
        ,   int& n                                                   /* The number of iterations */
        ,   double const delta = 0.001                               /* Absolute error tolerance */
        ,   double const epsilon = 0.001                             /* Relative error tolerance */
        );
    
    // Plain Monte Carlo Multi-Dimensional Integration
    static std::pair<double, double> plainMonteCarloIntegrator
        (   std::function<double(Vector)> const f                    /* The function we will integrate */
        ,   Vector const& a                                          /* The vector of start bounds */
        ,   Vector const& b                                          /* The vector of end bounds */
        ,   int const N                                              /* The number of points we will sample */
        );
    
    // Halton Lattice Low-Discrepancy Monte Carlo Integration
    static std::pair<double, double> haltonLatticeMonteCarloIntegrator
        (   std::function<double(Vector)> const f                    /* The function we will integrate */
        ,   Vector const& a                                          /* The vector of start bounds */
        ,   Vector const& b                                          /* The vector of end bounds */
        ,   int const N                                              /* The number of points we will sample */
        );

    // Error function implementation using integral form
    static double errorFunction(const double z);

    // Van der Corupt psuedo random number generator
    static double corputRandomNumberGenerator(int n, int b);

    // Halton pseudo random number generator
    static Vector haltonRandomNumberGenerator(int n, int dim);

    // Latice rule pseudo random number generator
    static Vector latticeRandomNumberGenerator(int n, int dim);

    // 2D Integration function
    static std::pair<double, double> twoDIntegration
    (   const std::function<double(double, double)> f            /* The function we will integrate */
    ,   const double a                                           /* Start of outter interval */
    ,   const double b                                           /* End of outer interval */
    ,   const std::function<double(double)> d                    /* Inner interval */
    ,   const std::function<double(double)> u                    /* Outer interval */
    ,   const double eps                                         /* Epsilon */
    ,   const double acc                                         /* Accuracy */
    ,   int& n                                                   /* Number of iterations called */
    ,   const bool clenshaw_curtis_transform = true              /* Toggle use of clenshaw transformation */
    );
};