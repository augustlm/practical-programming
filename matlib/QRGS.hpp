#pragma once

#include<functional>

class Matrix;

class Vector;

class QRGS
{
public:
    static void decomp(Matrix& a, Matrix& r);
    static Vector solve(const Matrix& Q, const Matrix& R, const Vector& b);
    static double det(const Matrix& R);
    static Matrix inverse(const Matrix& Q, const Matrix& R);
    static const std::pair<Vector, Matrix> lsfit(const std::function<double(double)> fs[], const int num_func, const Vector& x, const Vector& y, const Vector& dy);
};