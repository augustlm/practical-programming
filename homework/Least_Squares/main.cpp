#include<iostream>
#include<fstream>
#include<cmath>
#include<functional>
#include<utility>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<QRGS.hpp>

int main()
{
    // Testing the QR decomp for a tall matrix
    std::cout << "Testing QR decompoosition for a tall matrix: \n" << std::endl;
    int height = 15, width = 10;
    Matrix Q = Matrix::randomMatrixInt(height, width, 0, 250);
    Matrix A(Q);
    Matrix R(width, width);
    std::cout << "Here is an example of a tall matrix: \n" << Q << std::endl;
    QRGS::decomp(Q, R);
    std::cout << "Checking QR = A for a the tall matrix: " << (Matrix_product(Q, R) == A) << "\n" << std::endl;

    // Part A
    std::cout << "Output for part A: (see also decay.svg) \n" << std::endl;

    // Taking the experimental data
    double x_axis[9] = {1, 2, 3, 4, 6, 9, 10, 13, 15};
    double y_values[9] = {117, 100, 88, 72, 53, 29.5, 25.2, 15.2, 11.1};
    double dy_values[9] = {5, 5, 5, 4, 4, 3, 3, 2, 2};

    // Making vectors instead because they are cooler
    Vector t(x_axis, 9);
    Vector activity(y_values, 9);
    Vector d_activity(dy_values, 9);
    Vector ln_activity(activity);
    Vector d_ln_activity(d_activity);
    for (int i = 0; i < ln_activity.getSize(); i++)
    {
        ln_activity[i] = log(activity[i]);
        d_ln_activity[i] = d_activity[i] / activity[i];
    }
     
    // Now we make an array with a straight line (always return 1) and a linear line (always return x)
    // The coeffecients on these will correspond to a and lambda
    std::function<double(double)> fs[2] = 
    {
        [](double z) -> double {return 1; },
        [](double z) -> double {return z; }
    };

    // Do the least squares
    const std::pair<Vector, Matrix> least_sq = QRGS::lsfit(fs, 2, t, ln_activity, d_ln_activity);
    const Vector c = least_sq.first;

    // First we print the points
    std::ofstream outdata;
    outdata.open("experiment.data");
    for (int i = 0; i < t.getSize(); i++)
    {
        outdata << t[i] << " " << ln_activity[i] << " " << d_ln_activity[i] << std::endl;
    }
    outdata.close();

    // Now we define a linear function and make a load of points
    outdata.open("fit.data");
    for (double x = 1.0; x <= 15.0; x += 0.01)
    {
        double y = c[0] + c[1] * x;
        outdata << x << " " << y << std::endl;
    }
    outdata.close();

    // Calculating the half life from the least squares fit and comparing
    // Source for modern value: https://www.isotopes.gov/sites/default/files/2021-02/Ra-224.pdf
    const double half_life = - log(2.0) / c[1];
    const double true_half_life = 3.6319;
    std::cout << "Comparing the calculated half-life " << half_life << " to the modern value " << true_half_life << std::endl;
    std::cout << "They seem reasonably close but need more information to judge. \n" << std::endl;

    // Part B (modified least squares to return S matrix as well)
    std::cout << "Output for part B: \n" << std::endl;

    // We get the covariance matrix and use it to calculate the uncertainty on lambda
    const Matrix S = least_sq.second;
    const double uncertainty_c = sqrt(S[0][0]);
    const double uncertainty_lambda = sqrt(S[1][1]);

    // Use this to compare result
    std::cout << "Comparing the calculated half-life " << half_life << " to the modern value " << true_half_life << std::endl;
    std::cout << "The experimental uncertainty on half-life is: " << uncertainty_lambda << std::endl;
    std::cout << "Since the experimental half-life is outside this range, the experiment does not agree with modern measurements. \n" << std::endl;

    // Part C
    std::cout << "Output for part C: (see adv_decay.svg)" << std::endl;

    // Now we define a linear function and make a load of points (plus uncertainty)
    outdata.open("fit_plus.data");
    for (double x = 1.0; x <= 15.0; x += 0.01)
    {
        double y = (c[0] + uncertainty_c) + (c[1] + uncertainty_lambda) * x;
        outdata << x << " " << y << std::endl;
    }
    outdata.close();

    // Now we define a linear function and make a load of points (minus uncertainty)
    outdata.open("fit_minus.data");
    for (double x = 1.0; x <= 15.0; x += 0.01)
    {
        double y = (c[0] - uncertainty_c) + (c[1] - uncertainty_lambda) * x;
        outdata << x << " " << y << std::endl;
    }
    outdata.close();

    return 0;
}
