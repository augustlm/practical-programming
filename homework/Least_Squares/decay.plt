# Set the output file name and type
set terminal svg
set output "Decay.svg"

# Set the title and axis labels
set title "Linear Fit of ln of Activity of ThX vs Time"
set xlabel "Time (days)"
set ylabel "ln of Activity y of ThX (relative units)"

# Plot the two lines, one with error bars
plot "experiment.data" using 1:2:3 with yerrorbars lc "black" title "Experimental Data", \
     "fit.data" using 1:2 with lines lc "red" title "Linear Fit"
