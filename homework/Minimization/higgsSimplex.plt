set terminal svg
set output "HiggsSimplex.svg"

set xlabel "Energy"
set ylabel "Signal"

plot "higgs.data" using 1:2:3 with yerrorbars title "experimental data", \
     "fitSimplex.data" using 1:2 with lines title "fit"
