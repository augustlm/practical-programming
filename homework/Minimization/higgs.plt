set terminal svg
set output "Higgs.svg"

set xlabel "Energy"
set ylabel "Signal"

plot "higgs.data" using 1:2:3 with yerrorbars title "experimental data", \
     "fit.data" using 1:2 with lines title "fit"
