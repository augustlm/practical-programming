#include<iostream>
#include<fstream>
#include<functional>
#include<cmath>
#include<string>
#include<sstream>
#include<Minimization.hpp>
#include<Vector_class.hpp>
#include<Matrix_class.hpp>
#include<Generic_list.hpp>

using std::cout, std::endl, std::cerr;

int main()
{
    cout << "Testing part A:" << endl;

    cout << "\n\nTesting the gradient finder:" << endl;
    cout << "\nGradient of x^2 + 3 at x = 4:" << endl;
    const std::function<const double (const Vector&)> test1 = [](const Vector& x)
    {
        return x[0] * x[0] + 3;
    };
    Vector x1(1);
    x1[0] = 4;
    cout << "Numerical approximation: " << Minimization::finiteDifferenceGradient(test1, x1);
    cout << "Target gradient: " << 8.0 << endl;
    cout << "\nGradient of the Rosenbrock function at x = -3, y = -4" << endl;
    const std::function<const double (const Vector&)> rosenbrock = [](const Vector& x)
    {
        return std::pow((1 - x[0]), 2) + 100 * std::pow((x[1] - std::pow(x[0], 2)), 2);
    };
    Vector x2(2);
    x2[0] = -3;
    x2[1] = -4;
    Vector y2(2);
    y2[0] = -15608;
    y2[1] = -2600;
    cout << "Numerical approximation: " << Minimization::finiteDifferenceGradient(rosenbrock, x2);
    cout << "Target gradient: " << y2;

    cout << "\n\nTesting the Quasi Newton implementation:" << endl;
    cout << "\nMinimizing the Rosenbrock function:" << endl;
    cout << "Finding the minimum of the Rosenbrock function with initial guess of x = 3 and y = 3" << endl;
    Vector rosenbrockStart(2);
    rosenbrockStart[0] = 3;
    rosenbrockStart[1] = 3;
    int counter = 0;
    Vector rosenbrockAns = Minimization::quasiNewton(rosenbrock, rosenbrockStart, counter);
    cout << "x_opt = " << rosenbrockAns;
    cout << "f(x_opt) = " << rosenbrock(rosenbrockAns) << endl;
    cout << "iterations = " << counter << endl;
    const std::function<const double (const Vector&)> himmelblau = [](const Vector& x) 
    {
        return std::pow(std::pow(x[0], 2) + x[1] - 11, 2.0) + std::pow(x[0] + std::pow(x[1], 2) - 7, 2.0);
    };
    cout << "\nMinimizing the Himmelblau function:" << endl;
    cout << "Finding the minimum of the Himmelblau function with initial guess of x = 5 and y = 3" << endl;
    Vector himmelblauStart(2);
    himmelblauStart[0] = 5;
    himmelblauStart[1] = 3;
    int counterH = 0;
    Vector himmelblauAns = Minimization::quasiNewton(himmelblau, himmelblauStart, counterH);
    cout << "x_opt = " << himmelblauAns;
    cout << "f(x_opt) = " << himmelblau(himmelblauAns) << endl;
    cout << "iterations = " << counterH << endl;

    Generic_list<double> energy;
    Generic_list<double> signal;
    Generic_list<double> error;
    std::string line;
    std::ifstream inputFile("higgs.data");
    while (std::getline(inputFile, line)) 
    {
        if (line.empty())
            break;
        std::istringstream iss(line);
        std::string word;
        iss >> word;
        energy.add(std::stod(word));
        iss >> word;
        signal.add(std::stod(word));
        iss >> word;
        error.add(std::stod(word));
    }
    inputFile.close();
    const std::function<const double (const Vector&)> D = [energy, signal, error](const Vector& x)
    {
        double m = x[0];
        double gamma = x[1];
        double a = x[2];
        const std::function<const double (const double)> f = [m, gamma, a](const double y)
        {
            return a / (std::pow(y - m, 2) + std::pow(gamma, 2) / 4);
        };
        double sum = 0.0;
        for (int i = 0; i < energy.get_size(); i++)
        {
            sum += std::pow((f(energy[i]) - signal[i]) / error[i], 2);
        }
        return sum;
    };
    cout << "\n\nTesting part B:" << endl;
    cout << "\n\nFitting m, Gamma, A for Higgs boson experiment with m = 123, Gamma = 3 and A = 6" << endl;
    Vector higgsStart(3);
    higgsStart[0] = 123.0;
    higgsStart[1] = 3.0;
    higgsStart[2] = 6.0;
    int higgsCounter = 0;
    Vector higgsAns = Minimization::quasiNewton(D, higgsStart, higgsCounter);
    cout << "mass = " << higgsAns[0] << endl;
    cout << "iterations = " << higgsCounter << endl;  
    double m = higgsAns[0];
    double gamma = higgsAns[1];
    double a = higgsAns[2];
    const std::function<const double (const double)> f = [m, gamma, a](const double y)
    {
        return a / (std::pow(y - m, 2) + std::pow(gamma, 2) / 4);
    };

    double start = 100.0;
    double finish = 160.0;
    int num_points = 1000;
    Vector xs(num_points);
    Vector ys(num_points);
    double step = (finish - start) / (num_points - 1);
    for (int i = 0; i < num_points; i++)
    {
        double x = start + step * i;
        double y = f(x);
        xs[i] = x;
        ys[i] = y;
    }
    std::ofstream outputFile("fit.data");
    for (int i = 0; i < num_points; i++) 
    {
        outputFile << xs[i] << " " << ys[i] << "\n";
    }
    outputFile.close();

    cout << "\n\nTesting part C:" << endl;

    cout << "\n\nTesting the Downhill Simplex implementation:" << endl;
    cout << "\nMinimizing the Rosenbrock function:" << endl;
    cout << "Finding the minimum of the Rosenbrock function with initial guess of x = 3 and y = 3" << endl;
    rosenbrockStart[0] = 3;
    rosenbrockStart[1] = 3;
    counter = 0;
    rosenbrockAns = Minimization::downhillSimplex(rosenbrock, rosenbrockStart, counter);
    cout << "x_opt = " << rosenbrockAns;
    cout << "f(x_opt) = " << rosenbrock(rosenbrockAns) << endl;
    cout << "iterations = " << counter << endl;
    cout << "\nMinimizing the Himmelblau function:" << endl;
    cout << "Finding the minimum of the Himmelblau function with initial guess of x = 5 and y = 3" << endl;
    himmelblauStart[0] = 5;
    himmelblauStart[1] = 3;
    counterH = 0;
    himmelblauAns = Minimization::downhillSimplex(himmelblau, himmelblauStart, counterH);
    cout << "x_opt = " << himmelblauAns;
    cout << "f(x_opt) = " << himmelblau(himmelblauAns) << endl;
    cout << "iterations = " << counterH << endl;

    cout << "\nFitting m, Gamma, A for Higgs boson experiment with m = 123, Gamma = 3 and A = 6" << endl;
    higgsStart[0] = 123.0;
    higgsStart[1] = 3.0;
    higgsStart[2] = 6.0;
    higgsCounter = 0;
    Vector higgsAnsSimplex = Minimization::downhillSimplex(D, higgsStart, higgsCounter);
    cout << "mass = " << higgsAnsSimplex[0] << endl;
    cout << "iterations = " << higgsCounter << endl;  
    m = higgsAnsSimplex[0];
    gamma = higgsAnsSimplex[1];
    a = higgsAnsSimplex[2];

    start = 100.0;
    finish = 160.0;
    num_points = 1000;
    Vector xsSimplex(num_points);
    Vector ysSimplex(num_points);
    step = (finish - start) / (num_points - 1);
    for (int i = 0; i < num_points; i++)
    {
        double x = start + step * i;
        double y = f(x);
        xsSimplex[i] = x;
        ysSimplex[i] = y;
    }
    std::ofstream outputFileSimplex("fitSimplex.data");
    for (int i = 0; i < num_points; i++) 
    {
        outputFileSimplex << xsSimplex[i] << " " << ysSimplex[i] << "\n";
    }
    outputFileSimplex.close();
    return 0;
}
