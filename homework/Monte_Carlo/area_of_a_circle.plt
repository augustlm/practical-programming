# do all the pretty stuff
set terminal svg
set output "Area_of_a_circle.svg"
set xlabel "Number of points"
set ylabel "Actual Error"
set title "Scaling of Error With Number of Points Sampled When Calculating the Area of a Unit Circle"

# define functions
f(x) = a/sqrt(x)
g(x) = b/sqrt(x)

# suppress fit output
set fit quiet

# fit the functions
fit f(x) "area_of_a_circle.data" index 0 using 1:2 via a
fit g(x) "area_of_a_circle.data" index 0 using 1:3 via b

# plot the points and fits
plot "area_of_a_circle.data" index 0 using 1:2 with lines title "Estimated Error using plain MC", \
     "area_of_a_circle.data" index 0 using 1:3 with lines title "Actual Error using plain MC", \
     f(x) with lines title "Fit of 1 / sqrt(N) to Estimated error using plain MC", \
     g(x) with lines title "Fit of 1 / sqrt(N) to Actual error using plain MC"