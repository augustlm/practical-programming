# do all the pretty stuff
set terminal svg
set output "Volume_of_a_sphere_hf.svg"
set xlabel "Number of points"
set ylabel "Actual Error"
set title "Scaling of Error With Number of Points Sampled When Calculating the Volume of a Sphere (Pseudo-Random)"

# define functions
h(x) = c/sqrt(x)
i(x) = d/sqrt(x)

# suppress fit output
set fit quiet

# fit the functions
fit h(x) "volume_of_a_sphere.data" index 1 using 1:2 via c
fit i(x) "volume_of_a_sphere.data" index 1 using 1:3 via d

# plot the points and fits
plot "volume_of_a_sphere.data" index 1 using 1:2 with lines title "Estimated Error using Halton/Lattice MC", \
     "volume_of_a_sphere.data" index 1 using 1:3 with lines title "Actual Error using Halton/Lattice MC", \
     h(x) with lines title "Fit of 1 / sqrt(N) to Estimated error using Halton/Lattice MC", \
     i(x) with lines title "Fit of 1 / sqrt(N) to Actual error using Halton/Lattice MC"
