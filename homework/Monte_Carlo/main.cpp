#include<iostream>
#include<fstream>
#include<cmath>
#include<functional>
#include<Integration.hpp>
#include<Vector_class.hpp>

using std::cout, std::endl;

int main()
{
    // Define the function to integrate
    std::function<double(Vector)> f = [](Vector x) 
    {
        return x.norm() <= 1.0;
    };

    // Define the bounds of integration
    Vector a(2);
    Vector b(2);
    a[0] = -1.0;
    a[1] = -1.0;
    b[0] = 1.0;
    b[1] = 1.0;

    // Init the answer double
    double estimate = 0.0;
    double estimate_hl = 0.0;
    int best_N = 0;
    int best_N_hl = 0;

    // Define the number of sampling points
    const int N_min = 1000;
    const int N_max = 100000;
    const int N_step = 1000;

    // Open a file to write the data to
    std::ofstream data_file("area_of_a_circle.data");

    // Loop over the number of sampling points (standard)
    for (int N = N_min; N <= N_max; N += N_step)
    {
        // Calculate the integral using the plain Monte Carlo integrator
        std::pair<double, double> result = Integration::plainMonteCarloIntegrator(f, a, b, N);

        // Calculate the actual error
        double actual_error = std::abs(result.first - M_PI);

        // Write the data to the file
        data_file << N << "     " << result.second << "     " << actual_error << "     " << result.first << endl;

        // Update best guess
        if (actual_error < std::abs(estimate - M_PI))
        {
            estimate = result.first;
            best_N = N;
        }
    }

    data_file << "\n" << endl;

    // Loop over the number of sampling points (pseudo-random)
    for (int N = N_min; N <= N_max; N += N_step)
    {
        // Calculate the integral using the plain Monte Carlo integrator
        std::pair<double, double> result = Integration::haltonLatticeMonteCarloIntegrator(f, a, b, N);

        // Calculate the actual error
        double actual_error = std::abs(result.first - M_PI);

        // Write the data to the file
        data_file << N << "     " << result.second << "     " << actual_error << "     " << result.first << endl;

        // Update best guess
        if (actual_error < std::abs(estimate_hl - M_PI))
        {
            estimate_hl = result.first;
            best_N_hl = N;
        }
    }

    // Close the file
    data_file.close();

    // Define the bounds of integration
    Vector a2(3);
    Vector b2(3);
    a2[0] = -1.0;
    a2[1] = -1.0;
    a2[2] = -1.0;
    b2[0] = 1.0;
    b2[1] = 1.0;
    b2[2] = 1.0;

    // Init the answer double
    double estimate2 = 0.0;
    double estimate2_hl = 0.0;
    int best_N2 = 0;
    int best_N2_hl = 0;
    const double target = 4.0/3.0 * M_PI;

    // Open a file to write the data to
    std::ofstream data_file2("volume_of_a_sphere.data");

    // Loop over the number of sampling points
    for (int N = N_min; N <= N_max; N += N_step)
    {
        // Calculate the integral using the plain Monte Carlo integrator
        std::pair<double, double> result = Integration::plainMonteCarloIntegrator(f, a2, b2, N);

        // Calculate the actual error
        double actual_error = std::abs(result.first - target);

        // Write the data to the file
        data_file2 << N << "     " << result.second << "     " << actual_error << "     " << result.first << endl;

        // Update best guess
        if (actual_error < std::abs(estimate2_hl - target))
        {
            estimate2 = result.first;
            best_N2 = N;
        }
    }

    data_file2 << "\n" << endl;

    // Loop over the number of sampling points (pseudo-random)
    for (int N = N_min; N <= N_max; N += N_step)
    {
        // Calculate the integral using the plain Monte Carlo integrator
        std::pair<double, double> result = Integration::haltonLatticeMonteCarloIntegrator(f, a2, b2, N);
        
        // Calculate the actual error
        double actual_error = std::abs(result.first - target);

        // Write the data to the file
        data_file2 << N << "     " << result.second << "     " << actual_error << "     " << result.first << endl;

        // Update best guess
        if (actual_error < std::abs(estimate2_hl - target))
        {
            estimate2_hl = result.first;
            best_N2_hl = N;
        }
    }

    // Close the file
    data_file2.close();

    // Define a third function to integrate
    std::function<double(Vector)> f3 = [](Vector x) 
    {
        double cos_1 = std::cos(x[0]);
        double cos_2 = std::cos(x[1]);
        double cos_3 = std::cos(x[2]);
        double cos_product = cos_1 * cos_2 * cos_3;
        double cos_part = 1.0 / (1.0 - cos_product);
        return cos_part / std::pow(M_PI, 3);
    };

    // Define the bounds of integration
    Vector a3(3);
    Vector b3(3);
    a3[0] = 0.0;
    a3[1] = 0.0;
    a3[2] = 0.0;
    b3[0] = M_PI;
    b3[1] = M_PI;
    b3[2] = M_PI;

    // Init the answer double
    const double target2 = 1.3932039296856768591842462603255;

    // No plot. Just one run with high N
    int N_large = 1000000;
    std::pair<double, double> result = Integration::plainMonteCarloIntegrator(f3, a3, b3, N_large);
    double actual_error = std::abs(result.first - target2);

    cout << "Testing for part A:" << endl;
    cout << "\nArea of a unit circle:     " << M_PI << endl;
    cout << "Best estimate:             " << estimate << endl;
    cout << "Points used:               " << best_N << endl;
    cout << "Max Points used:           " << N_max << endl;
    cout << "\nSee also Area_of_circle.svg." << endl;
    cout << "Here it can be seen that the actual error scales roughly as 1/sqrt(N)." << endl;
    cout << "\nArea of a unit sphere:     " << target << endl;
    cout << "Best estimate:             " << estimate2 << endl;
    cout << "Points used:               " << best_N2 << endl;
    cout << "Max Points used:           " << N_max << endl;
    cout << "\nSee also Volume_of_sphere.svg." << endl;
    cout << "Here it can be seen that the actual error scales roughly as 1/sqrt(N)." << endl;
    cout << "\nAttempting the difficult integration:" << endl;
    cout << "Run with N = " << N_large << " gives abs error of " << actual_error << " and rel error of " << actual_error / target2 * 100 << "%" << endl;

    cout << "\nTesting for part B:" << endl;
    cout << "\nArea of a unit circle:     " << M_PI << endl;
    cout << "Best estimate:             " << estimate_hl << endl;
    cout << "Points used:               " << best_N_hl << endl;
    cout << "Max Points used:           " << N_max << endl;
    cout << "\nSee also Area_of_circle_hf.svg." << endl;
    cout << "Here it can be seen the scaling is better using pseudo-random sampling." << endl;
    cout << "\nArea of a unit sphere:     " << target << endl;
    cout << "Best estimate:             " << estimate2_hl << endl;
    cout << "Points used:               " << best_N2_hl << endl;
    cout << "Max Points used:           " << N_max << endl;
    cout << "\nSee also Volume_of_sphere_hf.svg." << endl;
    cout << "Here it can be seen the scaling is better using pseudo-random sampling." << endl;
    return 0;
}
