# do all the pretty stuff
set terminal svg
set output "Area_of_a_circle_hf.svg"
set xlabel "Number of points"
set ylabel "Actual Error"
set title "Scaling of Error With Number of Points Sampled When Calculating the Area of a Unit Circle (Pseudo-Random)"

# define functions
h(x) = c/sqrt(x)
i(x) = d/sqrt(x)

# suppress fit output
set fit quiet

# fit the functions
fit h(x) "area_of_a_circle.data" index 1 using 1:2 via c
fit i(x) "area_of_a_circle.data" index 1 using 1:3 via d

# plot the points and fits
plot "area_of_a_circle.data" index 1 using 1:2 with lines title "Estimated Error using Halton/Lattice MC", \
     "area_of_a_circle.data" index 1 using 1:3 with lines title "Actual Error using Halton/Lattice MC", \
     h(x) with lines title "Fit of 1 / sqrt(N) to Estimated error using Halton/Lattice MC", \
     i(x) with lines title "Fit of 1 / sqrt(N) to Actual error using Halton/Lattice MC"