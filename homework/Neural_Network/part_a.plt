set terminal svg
set output "Part_a.svg"

# Set plot properties
set title "Training a neural network to cos(5x - 1) * exp(-x^2)"
set xlabel "x"
set ylabel "y"

# Plot training points
plot "part_a_training.data" using 1:2 with points title "Training Points", \
     "part_a_plot.data" using 1:2 with lines title "Predicted Points"
