set terminal svg
set output "Part_b.svg"

# Set plot properties
set title "Training a neural network to cos(5x - 1) * exp(-x)"
set xlabel "x"
set ylabel "y"

# Plot training points
plot "part_b_training.data" index 0 using 1:2 with points pt 1 lc "black" title "Analytical Points", \
     "part_b_training.data" index 1 using 1:2 with points pt 1 lc "black" notitle, \
     "part_b_training.data" index 2 using 1:2 with points pt 1 lc "black" notitle, \
     "part_b_training.data" index 3 using 1:2 with points pt 1 lc "black" notitle, \
     "part_b_plot.data" index 0 using 1:2 with lines lc "blue" title "Predicted Function", \
     "part_b_plot.data" index 1 using 1:2 with lines lc "red" title "Predicted Derivative", \
     "part_b_plot.data" index 2 using 1:2 with lines lc "orange" title "Predicted Second Derivative", \
     "part_b_plot.data" index 3 using 1:2 with lines lc "green" title "Predicted Anti-Derivative"
