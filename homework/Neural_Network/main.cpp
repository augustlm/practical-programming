#include<iostream>
#include<functional>
#include<cmath>
#include<fstream>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<NeuralNetwork.hpp>

using std::cout, std::endl;

int main()
{
    // Testing for part A
    cout << "Testing for part A: " << endl;
    std::function<double(double)> analytical_aim = [](double x)
    {
        return std::cos(5 * x - 1) * std::exp(- std::pow(x, 2));
    };
    int numpoints = 20;
    cout << "\n\nGenerating " << numpoints << " training points for cos(5x - 1) * exp(-x^2) from analytical function:" << endl;
    Vector x(numpoints);
    Vector y(numpoints);
    double data_step = 2.0 / (double)(numpoints - 1);
    std::ofstream training_a("part_a_training.data");
    for (int i = 0; i < numpoints; i++)
    {
        x[i] = -1 + i * data_step; 
        y[i] = analytical_aim(x[i]);
        training_a << x[i] << " " << y[i] << endl;
    }
    training_a.close();
    cout << "x:" << x;
    cout << "y:" << y << endl;
    int neurons = 6;
    NeuralNetwork simple_network(neurons);
    simple_network.train(x, y);
    int plot_points = 10000;
    Vector plot_x_a(plot_points);
    Vector plot_y_a(plot_points);
    cout << "Training a neural network with " << neurons << " neurons using the Gaussian wavelet activation function" << endl;
    cout << "Results plotted in Part_a.svg" << endl;
    double plot_step = 2.0 / (double)(plot_points - 1);
    std::ofstream data_file_a("part_a_plot.data");
    for (int i = 0; i < plot_points; i++)
    {
        plot_x_a[i] = -1 + i * plot_step; 
        plot_y_a[i] = simple_network.response(plot_x_a[i]);
        data_file_a << plot_x_a[i] << " " << plot_y_a[i] << endl;
    }
    data_file_a.close();

    // Testing for part b
    cout << "\n\nTesting for part B:" << endl;
    analytical_aim = [](double x)
    {
        return std::cos(5 * x - 1) * std::exp(-x);
    };
    std::function<double(double)> analytical_derivative = [](double x)
    {
        return -std::exp(-x) * (std::cos(1 - 5 * x) - 5 * std::sin(1 - 5 * x));
    };
    std::function<double(double)> analytical_second_derivative = [](double x)
    {
        return -2 * (12 * std::cos(1 - 5 * x) + 5 * std::sin(1 - 5 * x)) * std::exp(-x);
    };
    std::function<double(double)> analytical_anti_derivative = [](double x)
    {
        return -1.0/26.0 * (std::cos(1 - 5 * x) + 5 * std::sin(1 - 5 * x)) * std::exp(-x);
    };
    int numpoints_b = 20;
    cout << "\nGenerating " << numpoints_b << " training points for cos(5x - 1) * exp(-x) from analytical function:" << endl;
    Vector x_b(numpoints_b);
    Vector y_b(numpoints_b);
    double data_step_b = 2.0 / (double)(numpoints_b - 1);
    std::ofstream training_b("part_b_training.data");
    for (int i = 0; i < numpoints_b; i++)
    {
        x_b[i] = -1 + i * data_step_b; 
        y_b[i] = analytical_aim(x_b[i]);
        training_b << x_b[i] << " " << y_b[i] << endl;
    }
    training_b << "\n\n";
    cout << "x:" << x_b;
    cout << "y:" << y_b;
    cout << "Also generating sample points for derivatives\n" << endl;
    int neurons_b = 6;
    NeuralNetwork simple_network_b(neurons_b);
    simple_network_b.train(x_b, y_b);
    int plot_points_b = 10000;
    for (int i = 0; i < numpoints_b; i++)
    {
        double x_der = -1 + i * data_step_b;
        double y_der = analytical_derivative(x_der);
        training_b << x_der << " " << y_der << endl;
    }
    training_b << "\n\n";
    for (int i = 0; i < numpoints_b; i++)
    {
        double x_der = -1 + i * data_step_b;
        double y_der = analytical_second_derivative(x_der);
        training_b << x_der << " " << y_der << endl;
    }
    training_b << "\n\n";
    for (int i = 0; i < numpoints_b; i++)
    {
        double x_der = -1 + i * data_step_b;
        double y_der = analytical_anti_derivative(x_der);
        training_b << x_der << " " << y_der << endl;
    }
    training_b.close();
    cout << "Training a neural network with " << neurons_b << " neurons using the Gaussian wavelet activation function" << endl;
    cout << "Results plotted in Part_b.svg along with predicted derivatives" << endl;
    cout << "Note the antiderivative has been shifted by -0.9223 as an integration constant" << endl;
    double plot_step_b = 2.0 / (double)(plot_points_b - 1);
    std::ofstream data_file_b("part_b_plot.data");
    for (int i = 0; i < plot_points_b; i++)
    {
        double x_b = -1 + i * plot_step_b; 
        double y_b = simple_network_b.response(x_b);
        data_file_b << x_b << " " << y_b << endl;
    }
    data_file_b << "\n\n";
    for (int i = 0; i < plot_points_b; i++)
    {
        double x_b = -1 + i * plot_step_b; 
        double y_b = simple_network_b.getDerivative(x_b);
        data_file_b << x_b << " " << y_b << endl;
    }
    data_file_b << "\n\n";
    for (int i = 0; i < plot_points_b; i++)
    {
        double x_b = -1 + i * plot_step_b; 
        double y_b = simple_network_b.getSecondDerivative(x_b);
        data_file_b << x_b << " " << y_b << endl;
    }
    data_file_b << "\n\n";
    for (int i = 0; i < plot_points_b; i++)
    {
        double x_b = -1 + i * plot_step_b; 
        double y_b = simple_network_b.getAntiDerivative(x_b);
        data_file_b << x_b << " " << y_b - 0.9223 << endl;
    }
    data_file_b.close();

    return 0;
}