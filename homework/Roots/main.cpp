#include<iostream>
#include<functional>
#include<cmath>
#include<fstream>
#include<Roots.hpp>
#include<Vector_class.hpp>
#include<Matrix_class.hpp>
#include<RungeKutta.hpp>
#include<Generic_list.hpp>


// Defining the M(E) function for part B
double hydrogen_calc(double r_min=0.001, double r_max=10, double acc=0.001, double eps=0.001)
{
    std::function<const Vector (const Vector&)> ME = [r_min, r_max, acc, eps](Vector E){
        std::function<Vector(double, Vector)> f = [E](double x, Vector y) {
            Vector res(2);
            res[0] = y[1];
            res[1] = -2.0 * (E[0] + 1.0 / x) * y[0];
            return res;
            };
        RungeKutta calc(f);
        Vector ya(2);
        ya[0] = r_min - r_min * r_min;
        ya[1] = 1 - 2 * r_min;
        double step_size = 0.01;
        std::pair<Generic_list<double>, Generic_list<Vector>> result = calc.driver(r_min, ya, r_max, step_size, acc, eps);
        Vector end_condition(1);
        end_condition[0] = result.second[0][0];
        return end_condition;
    };
    double E_start = -1.0;
    Vector start_guess(1);
    start_guess[0] = E_start;
    Vector opt_result = Roots::newtonBacktracking(ME, start_guess);
    return opt_result[0];
}

Vector hydrogen(double x, Vector y)
{
    const double energy = -0.5;
    Vector res(2);
    res[0] = y[1];
    res[1] = -2.0 * (energy + 1.0 / x) * y[0];
    return res;
}

int main()
{
    // Define the functions that we want to find the Jacobian of
    const std::function<const Vector (const Vector&)> double_elements = [](const Vector& x)
    {
        Vector ans(x.getSize());
        for (int i = 0; i < x.getSize(); i++)
        {
            ans[i] = 2 * x[i];
        };
        return ans;
    };

    const std::function<const Vector (const Vector&)> f = [](const Vector& x)
    {
        Vector ans(x.getSize());
        ans[0] = x[0] * x[0] + x[1] * x[1];
        ans[1] = x[0] * x[1];
        return ans;
    };

    // Testing for part A
    std::cout << "Part A: " << std::endl;
    std::cout << "\n\nTesting the implementaion of the Jacobian finder:" << std::endl;

    // Test case 1:
    double x1[2] = {1.0, 2.0};
    Vector x1_v(x1, 2);
    Matrix J1 = Roots::finiteDifferenceJacobian(f, x1_v);
    std::cout << "\nJacobian of y_0 = x_0^2 + x_1^2, y_1 = x_0 * x_1 at x = [1, 2]:\n" << J1 << std::endl;

    // Test case 2:
    double x2[3] = {1.0, 2.0, 3.0};
    Vector x2_v(x2, 3);
    Matrix J2 = Roots::finiteDifferenceJacobian(double_elements, x2_v);
    std::cout << "Jacobian of y = 2 * x at x = [1, 2, 3]:\n" << J2 << std::endl;

    // Functions for root finding
    const std::function<const Vector (const Vector&)> f_root = [](const Vector& x)
    {
        Vector ans(x.getSize());
        ans[0] = x[0] * 4 - 4.0;
        return ans;
    };

    const std::function<const Vector (const Vector&)> f_root_2 = [](const Vector& x)
    {
        Vector ans(x.getSize());
        ans[0] = 0.25 * x[0] * x[0] + 4.0 * x[0] - 6.0;
        return ans;
    };

    const std::function<const Vector (const Vector&)> f_root_3 = [](const Vector& x)
    {
        Vector ans(x.getSize());
        ans[0] = x[0] + 3 * x[1] * x[0] - 4;
        ans[1] = x[0] * x[1] - 2 * x[1] + 10;
        return ans;
    };

    // Testing the actual Root finder
    std::cout << "\nTesting the actual Root finder:" << std::endl;
    
    // Test case 1:
    std::cout << "\nFinding the root of y = 4x - 4 with initial guess of x = 4" << std::endl;
    Vector guess1(1);
    guess1[0] = 4.0;
    Vector ans1 = Roots::newtonBacktracking(f_root, guess1);
    std::cout << "x_opt = " << ans1[0] << std::endl;
    std::cout << "f(x_opt) = " << f_root(ans1) << std::endl;

    // Test case 2:
    std::cout << "Finding the root of y = 0.25x^2 + 4x -6 with initial guess of x = 5" << std::endl;
    Vector guess2(1);
    guess2[0] = 5.0;
    Vector ans2 = Roots::newtonBacktracking(f_root_2, guess2);
    std::cout << "x_opt = " << ans2[0] << std::endl;
    std::cout << "f(x_opt) = " << f_root_2(ans2) << std::endl;
    std::cout << "Finding the root of y = 0.25x^2 + 4x -6 with initial guess of x = -25" << std::endl;
    guess2[0] = -25.0;
    ans2 = Roots::newtonBacktracking(f_root_2, guess2);
    std::cout << "x_opt = " << ans2[0] << std::endl;
    std::cout << "f(x_opt) = " << f_root_2(ans2) << std::endl;

    // Test case 3:
    std::cout << "Finding the root of y_0 = 3 * x_0 * x_1 + x[0] + 4, y_1 = x_0 * x_1 - 2 * x_1 + 10 with initial guess of x = (1.5, 2.5)" << std::endl;
    Vector guess3(2);
    guess3[0] = 1.5;
    guess3[1] = 2.5;
    Vector ans3 = Roots::newtonBacktracking(f_root_3, guess3);
    std::cout << "x_opt = " << ans3;
    std::cout << "f(x_opt) = " << f_root_3(ans3) << std::endl;

    // Testing the Rosenbrock function
    const std::function<const Vector (const Vector&)> rosenbrock = [](const Vector x)
    {
        Vector res(2);
        res[0] = 2 * (200 * std::pow(x[0], 3) - 200 * x[0] * x[1] + x[0] - 1);
        res[1] = 200 * (x[1] - std::pow(x[0], 2));
        return res;
    };
    std::cout << "Finding the root of the Rosenbrock function with initial guess of x = (-3, -4)" << std::endl;
    Vector guess4(2);
    guess4[0] = -3;
    guess4[1] = -4;
    Vector ans4 = Roots::newtonBacktracking(rosenbrock, guess4);
    std::cout << "x_opt = " << ans4;
    std::cout << "f(x_opt) = " << rosenbrock(ans4) << std::endl;

    // Testing for part B
    std::cout << "\nPart B:" << std::endl;
    std::cout << "\n\nFirst we check if the ODE can solve the Schrodinger:\n" << std::endl;
    RungeKutta test(hydrogen);
    double r_min = 0.001;
    Vector ya(2);
    ya[0] = r_min - r_min * r_min;
    ya[1] = 1 - 2 * r_min;
    double r_max = 10;
    double step_size = 0.01;
    double acc = 0.001;
    double eps = 0.001;
    std::pair<Generic_list<double>, Generic_list<Vector>> hydrogen_ans = test.driver(r_min, ya, r_max, step_size, acc, eps);
    std::cout << "Estimated f(" << hydrogen_ans.first[0] << ") = " << hydrogen_ans.second[0][0] << std::endl;
    std::cout << "Exact f(" << hydrogen_ans.first[0] << ") = " << hydrogen_ans.first[0] * std::exp(-hydrogen_ans.first[0]) << std::endl;
    double baseline = hydrogen_calc();
    std::cout << "\n\nNow we optimise M(E) with a start guess E = -1.0 and safe r_min, r_max, acc and eps:\n" << std::endl;
    std::cout << "Optimised E = " << baseline << std::endl;
    std::cout << "Target E = " << -0.5 << std::endl;
    std::cout << "\n\nMaking files and plots testing convergence of r_min, r_max, acc and eps" << std::endl;
    
    int num_points = 10;
    
    // Data file for r_min convergence series
    std::ofstream file_r_min("convergence_r_min.data");
    double r_min_start = 0.001;
    double r_min_end = 0.5;
    double r_min_step = (r_min_end - r_min_start) / (num_points - 1);
    for (int i = 0; i < num_points; ++i) {
        double r_min = r_min_start + i * r_min_step;
        double result = hydrogen_calc(r_min);
        file_r_min << r_min << "     " << result << std::endl;
    }
    file_r_min.close();
    
    // Data file for r_max convergence series
    std::ofstream file_r_max("convergence_r_max.data");
    double r_max_start = 3.0;
    double r_max_end = 10.0;
    double r_max_step = (r_max_end - r_max_start) / (num_points - 1);
    for (int i = 0; i < num_points; ++i) {
        double r_max = r_max_start + i * r_max_step;
        double result = hydrogen_calc(0.001, r_max);
        file_r_max << r_max << "     " << result << std::endl;
    }
    file_r_max.close();
    
    // Data file for acc convergence series
    std::ofstream file_acc("convergence_acc.data");
    if (!file_acc) {
        std::cerr << "Failed to create file: convergence_acc.txt" << std::endl;
        return 1;
    }
    double acc_start = 0.001;
    double acc_end = 1.0;
    double acc_step = (acc_end - acc_start) / (num_points - 1);
    for (int i = 0; i < num_points; ++i) {
        double acc = acc_start + i * acc_step;
        double result = hydrogen_calc(0.001, 10, acc);
        file_acc << acc << "     " << result << std::endl;
    }
    file_acc.close();
    
    // Data file for eps convergence series
    std::ofstream file_eps("convergence_eps.data");
    double eps_start = 0.001;
    double eps_end = 0.5;
    double eps_step = (eps_end - eps_start) / (num_points - 1);
    for (int i = 0; i < num_points; ++i) {
        double eps = eps_start + i * eps_step;
        double result = hydrogen_calc(0.001, 10, 0.001, eps);
        file_eps << eps << "     " << result << std::endl;
    }
    file_eps.close();
    
    std::cout << "Convergence data files created successfully." << std::endl;
    std::cout << "\n\nOptimal parameters: " << std::endl;
    std::cout << "acc: 0.001" << std::endl;
    std::cout << "eps: 0.001" << std::endl;
    std::cout << "r_min: 0.05" << std::endl;
    std::cout << "r_max: 7" << std::endl;
    std::cout << "Result with these parameters: " << hydrogen_calc(0.05, 7, 0.001, 0.001) << std::endl;
    return 0;
}
