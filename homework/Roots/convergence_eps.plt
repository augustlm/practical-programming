set terminal svg
set output 'Convergence_eps.svg'

set xlabel 'Epsilon'
set ylabel 'Energy'

set title 'Convergence Series for Epsilon'

# Plot data points
plot 'convergence_eps.data' using 1:2 with linespoints notitle, \
     -0.5 with lines lt 2 lc rgb 'red' notitle