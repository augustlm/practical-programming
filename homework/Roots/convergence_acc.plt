set terminal svg
set output 'Convergence_acc.svg'

set xlabel 'Accuracy'
set ylabel 'Energy'

set title 'Convergence Series for Accuracy'

# Plot data points
plot 'convergence_acc.data' using 1:2 with linespoints notitle, \
     -0.5 with lines lt 2 lc rgb 'red' notitle
