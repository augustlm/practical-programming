set terminal svg
set output 'Convergence_r_max.svg'

set xlabel 'r_{max}'
set ylabel 'Energy'

set title 'Convergence Series for r_max'

# Plot data points
plot 'convergence_r_max.data' using 1:2 with linespoints notitle, \
     -0.5 with lines lt 2 lc rgb 'red' notitle