set terminal svg
set output 'Convergence_r_min.svg'

set xlabel 'r_{min}'
set ylabel 'Energy'

set title 'Convergence Series for r_min'

# Plot data points
plot 'convergence_r_min.data' using 1:2 with linespoints notitle, \
     -0.5 with lines lt 2 lc rgb 'red' notitle