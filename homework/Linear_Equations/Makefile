# Compiler
CXX = g++

# Compiler flags
CXXFLAGS = -Wall

# Directories to include
MATLIB_DIR = ../../matlib/

# Includes
INCLUDE = $(MATLIB_DIR)

# Source files
SRCS = Matrix_class.cpp Vector_class.cpp QRGS.cpp

#  Object directory
OBJS_DIR = objects/

# Object files
OBJS = $(addprefix $(OBJS_DIR), $(SRCS:.cpp=.o))

# Output files
OUT1 = Out.txt
OUT2 = Time_plot.svg

.PHONY: all clean

all: $(OUT1) $(OUT2)

$(OUT1): main.x
	./$< > $@

$(OUT2) : time_plot.plt time.data
	gnuplot $<

# Data files

time.data : time_test.x
	>$@
	for N in $$(seq 100 100 1000); do \
		time --format "$$N %e" --output $@ --append \
		./$< $$N ;\
	done

# Executables

main.x: $(OBJS_DIR)main.o $(OBJS)
	$(CXX) $(CXXFLAGS) $^ -o $@

time_test.x: $(OBJS_DIR)time_test.o $(OBJS)
	$(CXX) $(CXXFLAGS) $^ -o $@

# Object files

$(OBJS_DIR)main.o: main.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)time_test.o: time_test.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)QRGS.o: $(MATLIB_DIR)QRGS.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)Vector_class.o: $(MATLIB_DIR)Vector_class.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)Matrix_class.o: $(MATLIB_DIR)Matrix_class.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

# Directories

$(OBJS_DIR):
	mkdir -p $(OBJS_DIR)

clean:
	rm -rf $(OBJS_DIR) *.x *.log *.data $(OUT1) $(OUT2)
