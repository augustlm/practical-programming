# Set the output file name and type
set terminal svg
set output 'Time_plot.svg'

# Set the title and axis labels
set title 'QR Decomposition Time vs Matrix Size With N^3 Fit'
set xlabel 'Matrix Size'
set ylabel 'Time (ms)'

# Plot the data and fit it to a curve of the form N^3
set fit quiet
f(x) = a*x**3
fit f(x) 'time.data' using 1:2 via a
plot 'time.data' using 1:2 with points pt 2 ps 1 lc 'red' title 'Data', f(x) with lines ls 1 lc 'blue' title 'N^3 Fit'
