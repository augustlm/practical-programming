#include<iostream>
#include<fstream>
#include<cstdlib>
#include<ctime>
#include"Matrix_class.hpp"
#include"Vector_class.hpp"
#include"QRGS.hpp"

int main()
{
    // Tesing part 1
    srand(time(0));
    int rows = rand() % 10 + 11;
    int cols = rand() % 10 + 1;
    Matrix mat(rows, cols);
    for (int i = 0; i < mat.getRows(); i++)
    {
        for (int j = 0; j < mat.getCols(); j++)
        {
            mat[i][j] = rand() % 100;
        }
    }
    std::cout << "An example of a matrix with " << mat.getRows() << " rows and " << mat.getCols() << " columns:" << std::endl;
    std::cout << "\n" << mat << std::endl;
    Matrix target(cols, cols);
    QRGS::decomp(mat, target);
    std::cout << "Decomp of the matrix:" << std::endl;
    std::cout << "\nQ:" << std::endl;
    std::cout << "\n" << mat << std::endl;
    std::cout << "R:" << std::endl;
    std::cout << "\n" << target << std::endl;
    std::cout << "Note that it is an upper triangle." << std::endl;
    Matrix trans = transpose(mat);
    std::cout << "\nThe product of Q^T and Q is: " << std::endl;
    std::cout << "\n" << Matrix_product(trans, mat) << std::endl;
    std::cout << "Note that it is the identity matrix." << std::endl;
    std::cout << "\nThe product of QR:\n\n" << Matrix_product(mat, target) << std::endl;
    std::cout << "Note that this is the original matrix." << std::endl;
    std::cout << "\nAll tests for Decomp passed." << std::endl;

    // Tesing part 2
    int dim = rand() % 10 + 11;
    Matrix sqr_mat(dim, dim);
    for (int i = 0; i < sqr_mat.getRows(); i++)
    {
        for (int j = 0; j < sqr_mat.getCols(); j++)
        {
            sqr_mat[i][j] = rand() % 100;
        }
    }
    Matrix sqr_target(dim, dim);
    Vector b(dim);
    for (int i = 0; i < b.getSize(); i++)
    {
        b[i] = rand() % 100;
    }
    std::cout << "\nGenerating a new square matrix of size " << dim << std::endl;
    std::cout << "\n" << sqr_mat << std::endl;
    std::cout << "Generating a vector with same size " << std::endl;
    std::cout << "\n" << b << std::endl;
    std::cout << "Decomping the square matrix" << std::endl;
    Matrix matrix_save(sqr_mat);
    QRGS::decomp(sqr_mat, sqr_target);
    std::cout << "\nQ:" << std::endl;
    std::cout << "\n" << sqr_mat << std::endl;
    std::cout << "R:" << std::endl;
    std::cout << "\n" << sqr_target << std::endl;
    std::cout << "Solving QRx=b " << std::endl;
    Vector ans = QRGS::solve(sqr_mat, sqr_target, b);
    std::cout << "\nQRx:" << std::endl;
    std::cout << "\n" << Matrix_Vector_product(sqr_mat, Matrix_Vector_product(sqr_target, ans)) << std::endl;
    std::cout << "b:" << std::endl;
    std::cout << "\n" << b << std::endl;
    std::cout << "Testing that Ax = b" << std::endl;
    Vector test = Matrix_Vector_product(matrix_save, ans);
    std::cout << "\nAx:\n" << std::endl;
    std::cout << test << std::endl;
    std::cout << "b:\n" << std::endl;
    std::cout << b << std::endl;
    std::cout << "All tests for Solve passed." << std::endl;

    // Testing part 3
    dim = 3;
    std::cout << "\nGenerating a new small square matrix of size " << dim << std::endl;
    Matrix sqr_mat_2(dim, dim);
    for (int i = 0; i < sqr_mat_2.getRows(); i++)
    {
        for (int j = 0; j < sqr_mat_2.getCols(); j++)
        {
            sqr_mat_2[i][j] = rand() % 100;
        }
    }
    Matrix sqr_target_2(dim, dim);
    std::cout << "\nDecomping the square matrix" << std::endl;
    std::cout << "\n" << sqr_mat_2 << std::endl;
    QRGS::decomp(sqr_mat_2, sqr_target_2);
    std::cout << "Q:" << std::endl;
    std::cout << "\n" << sqr_mat_2 << std::endl;
    std::cout << "R:" << std::endl;
    std::cout << "\n" << sqr_target_2 << std::endl;
    std::cout << "The det of R is " << QRGS::det(sqr_target_2) << std::endl;
    std::cout << "\nCheck by hand" << std::endl;
    std::cout << "\nAll tests for Det passed." << std::endl;

    // Testing part 4
    dim = rand() % 10 + 11;
    Matrix inv_test_mat(dim, dim);
    for (int i = 0; i < inv_test_mat.getRows(); i++)
    {
        for (int j = 0; j < inv_test_mat.getCols(); j++)
        {
            inv_test_mat[i][j] = rand() % 100;
        }
    }
    std::cout << "\nGenerating a new square matrix of size " << dim << std::endl;
    std::cout << "\n" << inv_test_mat << std::endl;
    std::cout << "Decomping (not shown) " << std::endl;
    Matrix inv_r_mat(dim, dim);
    Matrix save_org(inv_test_mat);
    QRGS::decomp(inv_test_mat, inv_r_mat);
    std::cout << "\nCalculating the inverse " << std::endl;
    Matrix inv_target = QRGS::inverse(inv_test_mat, inv_r_mat);
    std::cout << "\nInverse matrix:" << std::endl;
    std::cout << "\n" << inv_target << std::endl;
    std::cout << "Testing that the inverse returns identity" << std::endl;
    std::cout << "\n" << Matrix_product(save_org, inv_target) << std::endl;

    return 0;
}
