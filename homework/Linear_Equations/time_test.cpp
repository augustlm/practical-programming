#include<fstream>
#include<cstdlib>
#include<ctime>
#include"Matrix_class.hpp"
#include"Vector_class.hpp"
#include"QRGS.hpp"

int main(int argc, char* argv[])
{
    srand(time(0));
    int size = std::stoi(argv[1]);
    Matrix mat(size, size);
    Matrix mat_r(size, size);
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            mat[i][j] = rand() % 100;
        }
    }
    QRGS::decomp(mat, mat_r);
}