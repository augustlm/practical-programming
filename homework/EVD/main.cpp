#include<iostream>
#include<cstdlib>
#include<ctime>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<EVD.hpp>

int main()
{
    srand(time(0));

    // Tesing unoptimised
    std::cout << "Testing the unoptimised EVD: \n" << std::endl;
    int size = 15;
    Matrix A(size, size);
    for (int i = 0; i < size; i++)
    {
        for (int j = i; j < size; j++)
        {
            A[i][j] = rand() % 100;
            A[j][i] = A[i][j];
        }
    }
    std::cout << "An example of a square symmetric matrix with size " << size << ":\n" << A << std::endl;
    EVD evd(A, false);
    Matrix V = evd.getEigenvectors();
    Matrix V_trans = transpose(V);
    Vector w = evd.getEigenvalues();
    Matrix D(size, size);
    for (int i = 0; i < size; i++)
    {
        D[i][i] = w[i];
    }
    Matrix VTAV = Matrix_product(V_trans, Matrix_product(A, V));
    Matrix VDVT = Matrix_product(V, Matrix_product(D, V_trans));
    Matrix I = Matrix::identity(size);
    Matrix VTV = Matrix_product(V_trans, V);
    Matrix VVT = Matrix_product(V, V_trans);
    std::cout << "Testing V^T A V == D:\n" << std::endl;
    std::cout << "V^T A V:\n" << VTAV << std::endl;
    std::cout << "D:\n" << D << std::endl;
    std::cout << "V^T A V == D: " << (VTAV == D) << std::endl;
    std::cout << "\nOther tests are only shown as bools.\n" << std::endl;
    std::cout << "V D V^T == A: " << (VDVT == A) << std::endl;
    std::cout << "V^T V == 1: " << (VTV == I) << std::endl; 
    std::cout << "V V^T == 1: " << (VVT == I) << std::endl;
    std::cout << "\nAll checks for unoptimised EVD complete :)" << std::endl;

    // Testing optimised 
    std::cout << "Testing the optimised EVD: \n" << std::endl;
    for (int i = 0; i < size; i++)
    {
        for (int j = i; j < size; j++)
        {
            A[i][j] = rand() % 100;
            A[j][i] = A[i][j];
        }
    }
    std::cout << "An example of a square symmetric matrix with size " << size << ":\n" << A << std::endl;
    EVD evd_2(A);
    V = evd_2.getEigenvectors();
    V_trans = transpose(V);
    w = evd_2.getEigenvalues();
    for (int i = 0; i < size; i++)
    {
        D[i][i] = w[i];
    }
    VTAV = Matrix_product(V_trans, Matrix_product(A, V));
    VDVT = Matrix_product(V, Matrix_product(D, V_trans));
    VTV = Matrix_product(V_trans, V);
    VVT = Matrix_product(V, V_trans);
    std::cout << "Testing V^T A V == D:\n" << std::endl;
    std::cout << "V^T A V:\n" << VTAV << std::endl;
    std::cout << "D:\n" << D << std::endl;
    std::cout << "V^T A V == D: " << (VTAV == D) << std::endl;
    std::cout << "\nOther tests are only shown as bools.\n" << std::endl;
    std::cout << "V D V^T == A: " << (VDVT == A) << std::endl;
    std::cout << "V^T V == 1: " << (VTV == I) << std::endl; 
    std::cout << "V V^T == 1: " << (VVT == I) << std::endl;
    std::cout << "\nAll checks for optimised EVD complete :)" << std::endl;
    return 0;
}
