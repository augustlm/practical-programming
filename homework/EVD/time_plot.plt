# Set the output file name and type
set terminal svg
set output 'Time_plot.svg'

# Set the title and axis labels
set title 'EVD Time vs Matrix Size With N^3 Fit'
set xlabel 'Matrix Size'
set ylabel 'Time (ms)'
set key top left

# Plot the data and fit it to a curve of the form N^3
set fit quiet
f(x) = a*x**3
g(x) = b*x**3
fit f(x) 'time.data' index 0 using 1:2 via a
fit g(x) 'time.data' index 1 using 1:2 via b
plot 'time.data' index 0 using 1:2 with points pt 2 ps 1 lc 'red' title 'Optimised times', \
     f(x) with lines ls 1 lc 'blue' title 'Optimised N^3 Fit', \
     'time.data' index 1 using 1:2 with points pt 2 ps 1 lc 'green' title 'Unoptimised time', \
     g(x) with lines ls 1 lc 'black' title 'Unoptimised N^3 Fit'

