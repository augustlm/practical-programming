set terminal svg background "white"
set output "Func_dr.svg"

# Set plot title and axis labels
set xlabel "r"
set ylabel "f(r)"
set title "Plot of the Hydrogen Ground State for Various Step Sizes"
set xrange [0:5]
set yrange [0:0.6]

# Analytical expression for Hydrogen
set samples 1000
a = 0.529177210903  # Bohr radius
f(x) = (1 / sqrt(pi)) * exp(-(x))

# Plot some data sets
plot f(x) with lines lw 2 title "Analytical expression", \
     "func_dr.data" index 0 using 1:2 with lines lw 2 title "{/Symbol D}r: 0.05", \
     "func_dr.data" index 1 using 1:2 with lines lw 2 title "{/Symbol D}r: 0.15", \
     "func_dr.data" index 2 using 1:2 with lines lw 2 title "{/Symbol D}r: 0.25", \
     "func_dr.data" index 3 using 1:2 with lines lw 2 title "{/Symbol D}r: 0.35", \
     "func_dr.data" index 4 using 1:2 with lines lw 2 title "{/Symbol D}r: 0.45"
