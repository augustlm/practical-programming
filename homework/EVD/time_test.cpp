#include<fstream>
#include<cstdlib>
#include<ctime>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<EVD.hpp>

int main(int argc, char* argv[])
{
    srand(time(0));
    int size = std::stoi(argv[1]);
    const int opt = std::stoi(argv[2]);
    Matrix A(size, size);
    for (int i = 0; i < size; i++)
    {
        for (int j = i; j < size; j++)
        {
            A[i][j] = rand() % 100;
            A[j][i] = A[i][j];
        }
    }
    EVD evd(A, opt);
    return 0;
}
