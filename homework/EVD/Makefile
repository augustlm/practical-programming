# Compiler
CXX = g++

# Compiler flags
CXXFLAGS = -Wall

# Directories to include
MATLIB_DIR = ../../matlib/

# Includes
INCLUDE = $(MATLIB_DIR)

# Source files
SRCS = Matrix_class.cpp Vector_class.cpp EVD.cpp

#  Object directory
OBJS_DIR = objects/

# Object files
OBJS = $(addprefix $(OBJS_DIR), $(SRCS:.cpp=.o))

# Output files
OUT1 = Out.txt
OUT2 = Time_plot.svg
OUT3 = Energies_rmax.svg
OUT4 = Energies_dr.svg
OUT5 = Func_dr.svg

.PHONY: all clean

all: $(OUT1) $(OUT2) $(OUT3) $(OUT4) $(OUT5)

$(OUT1) : main.x
	./$< > $@

$(OUT2) : time_plot.plt time.data
	gnuplot $<

$(OUT3) : energies_rmax.plt energies_rmax.data
	gnuplot $<

$(OUT4) : energies_dr.plt energies_dr.data
	gnuplot $<

$(OUT5) : func_dr.plt func_dr.data
	gnuplot $<

# Data files

HYDROGEN = hydrogen_calc.x
MAKEFILE = Makefile

# Timing data loop

time.data : time_test.x $(MAKEFILE)
	>$@
	echo "\"optimised times\"" >> $@
	for N in $$(seq 50 25 250); do \
		time --format "$$N %e" --output $@ --append \
		./$< $$N 1 ;\
	done 
	echo >> $@
	echo >> $@
	echo "\"unoptimised times\"" >> $@
	for N in $$(seq 50 25 250); do \
		time --format "$$N %e" --output $@ --append \
		./$< $$N 0 ;\
	done

# Parallelized energy_rmax.data generation

PREFIX = energies_rmax_

energies_rmax.data : $(PREFIX)1.data $(PREFIX)2.data $(PREFIX)3.data $(PREFIX)4.data $(PREFIX)5.data $(PREFIX)6.data $(PREFIX)7.data $(PREFIX)8.data $(PREFIX)9.data $(PREFIX)10.data
	cat $^ > $@
	rm $^

$(PREFIX)1.data : $(HYDROGEN) $(MAKEFILE)
	./$< 1.00 0.3 M > $@

$(PREFIX)2.data : $(HYDROGEN) $(MAKEFILE)
	./$< 2.50 0.3 M > $@

$(PREFIX)3.data : $(HYDROGEN) $(MAKEFILE)
	./$< 5.00 0.3 M > $@

$(PREFIX)4.data : $(HYDROGEN) $(MAKEFILE)
	./$< 7.50 0.3 M > $@

$(PREFIX)5.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10.0 0.3 M > $@

$(PREFIX)6.data : $(HYDROGEN) $(MAKEFILE)
	./$< 12.5 0.3 M > $@

$(PREFIX)7.data : $(HYDROGEN) $(MAKEFILE)
	./$< 15.0 0.3 M > $@

$(PREFIX)8.data : $(HYDROGEN) $(MAKEFILE)
	./$< 17.5 0.3 M > $@

$(PREFIX)9.data : $(HYDROGEN) $(MAKEFILE)
	./$< 20.0 0.3 M > $@

$(PREFIX)10.data : $(HYDROGEN) $(MAKEFILE)
	./$< 22.5 0.3 M > $@

# Parallelized energy_dr.data generation

PREFIX = energies_dr_

energies_dr.data : $(PREFIX)1.data $(PREFIX)2.data $(PREFIX)3.data $(PREFIX)4.data $(PREFIX)5.data $(PREFIX)6.data $(PREFIX)7.data $(PREFIX)8.data $(PREFIX)9.data $(PREFIX)10.data
	cat $^ > $@
	rm $^

$(PREFIX)1.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.05 R > $@

$(PREFIX)2.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.10 R > $@

$(PREFIX)3.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.15 R > $@

$(PREFIX)4.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.20 R > $@

$(PREFIX)5.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.25 R > $@

$(PREFIX)6.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.30 R > $@

$(PREFIX)7.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.35 R > $@

$(PREFIX)8.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.40 R > $@

$(PREFIX)9.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.45 R > $@

$(PREFIX)10.data : $(HYDROGEN) $(MAKEFILE)
	./$< 10 0.50 R > $@

# Parallelized func_dr.data generation

PREFIX = func_dr_

func_dr.data : $(PREFIX)1.data $(PREFIX)2.data $(PREFIX)3.data $(PREFIX)4.data $(PREFIX)5.data
	cat $^ > $@
	rm $^

$(PREFIX)1.data : $(HYDROGEN) $(MAKEFILE)
	./$< 15 0.05 F > $@

$(PREFIX)2.data : $(HYDROGEN) $(MAKEFILE)
	./$< 15 0.15 F > $@

$(PREFIX)3.data : $(HYDROGEN) $(MAKEFILE)
	./$< 15 0.25 F > $@

$(PREFIX)4.data : $(HYDROGEN) $(MAKEFILE)
	./$< 15 0.35 F > $@

$(PREFIX)5.data : $(HYDROGEN) $(MAKEFILE)
	./$< 15 0.45 F > $@

# Executables

main.x: $(OBJS_DIR)main.o $(OBJS)
	$(CXX) $(CXXFLAGS) $^ -o $@

time_test.x: $(OBJS_DIR)time_test.o $(OBJS)
	$(CXX) $(CXXFLAGS) $^ -o $@

hydrogen_calc.x: $(OBJS_DIR)hydrogen_calc.o $(OBJS)
	$(CXX) $(CXXFLAGS) $^ -o $@

# Object files

$(OBJS_DIR)main.o: main.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)time_test.o: time_test.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)hydrogen_calc.o: hydrogen_calc.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)EVD.o: $(MATLIB_DIR)EVD.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)Vector_class.o: $(MATLIB_DIR)Vector_class.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

$(OBJS_DIR)Matrix_class.o: $(MATLIB_DIR)Matrix_class.cpp | $(OBJS_DIR)
	$(CXX) -I$(INCLUDE) $(CXXFLAGS) -c $< -o $@

# Directories

$(OBJS_DIR):
	mkdir -p $(OBJS_DIR)

# Cleaning

clean:
	rm -rf $(OBJS_DIR) *.x *.data *.log $(OUT1) $(OUT2) $(OUT3) $(OUT4) $(OUT5)
