#include<iostream>
#include<fstream>
#include<cstdlib>
#include<cmath>
#include<Matrix_class.hpp>
#include<Vector_class.hpp>
#include<EVD.hpp>

int main(int argc, char *argv[])
{
    // Keyword time
    if (argc != 4) 
    {
        std::cerr << "Usage: " << argv[0] << " <int: rmax> <double: dr> <str: E/F>" << std::endl;
        return 1;
    }

    // init some variables
    const double rmax = std::stof(argv[1]);
    const double dr = std::stof(argv[2]);
    const char out = *argv[3];
    int npoints = (int)(rmax/dr)-1;
    Vector r(npoints);
    for(int i = 0; i < npoints; i++)
    {
        r[i] = dr * (i + 1);
    }

    // make the Hamiltonian
    Matrix H(npoints, npoints);
    for(int i = 0; i < npoints - 1; i++)
    {
        H[i][i] = -2;
        H[i][i+1] = 1;
        H[i+1][i] = 1;
    }
    H[npoints-1][npoints-1] = -2;
    H.scale(-0.5 / dr / dr);
    for(int i = 0; i < npoints; i++)
    {
        H[i][i] += -1 / r[i];
    }
    
    // diagonalise it and get the lowest energy and eigenvectors
    EVD evd(H);
    double lowest_energy = evd.getEigenvalues()[0];
    Matrix eigen_vectors = evd.getEigenvectors();

    // open file in append mode and write the energy value to it
    if (out == 'R')
    {
        std::cout << dr << "    " << lowest_energy << std::endl;
        return 0;
    }
    else if (out == 'M')
    {
        std::cout << rmax << "    " << lowest_energy << std::endl;
        return 0;
    }
    else if (out == 'F')
    {
        std::cout << "\"dr: " << dr << "\"" << std::endl;
        Vector output(eigen_vectors.getCols());
        double norm = 0.0;
        for (int row = 0; row < npoints; row++)
        {
            double x = r[row];
            double y = eigen_vectors[row][0];
            y /= x;
            norm += 4 * M_PI * x * x * dr * y * y;
            output[row] = y;
        }
        for (int row = 0; row < npoints; row++)
        {
            double x = r[row]; 
            double y = output[row];
            y /= std::sqrt(norm);
            std::cout << x << "   " << y << std::endl;
        }
        std::cout << "\n \n";
        return 0;
        
    }
    
}
