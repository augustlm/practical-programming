set terminal svg background "white"
set output "Energies_rmax.svg"

# Set plot title and axis labels
set title "Ground State Energy vs r_{max}"
set xlabel "r_{max}"
set ylabel "Ground State Energy"

# Set plot style for data points
set style line 1 pt 2 ps 1.5 lc "red"

# Set x and y range
set xrange [0:25]
set yrange [-0.5:-0.47]

# Plot the data
plot "energies_rmax.data" using 1:2 with points ls 1 title "Ground State Energy"
