set terminal svg background "white"
set output "Energies_dr.svg"

# Set plot title and axis labels
set title "Ground State Energy vs {/Symbol D}r"
set xlabel "{/Symbol D}r"
set ylabel "Ground State Energy"

# Set plot style for data points
set style line 1 pt 2 ps 1 lc "red"

# Set x and y range
set xrange [-0.05:0.6]
set yrange [-0.51:-0.45]

# Plot the data
plot "energies_dr.data" using 1:2 with points ls 1 title "Ground State Energy"
