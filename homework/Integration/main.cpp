#include<iostream>
#include<functional>
#include<cmath>
#include<fstream>
#include<limits>
#include<Integration.hpp>
#include<Vector_class.hpp>

using std::cout, std::endl;

double error_function(const double x) 
{
    if (x < 0) 
    {
        return -error_function(-x);
    }
    double coefficients[] = {0.254829592, -0.284496736, 1.421413741, -1.453152027, 1.061405429};
    double t = 1 / (1 + 0.3275911 * x);
    double sum = t * (coefficients[0] + t * (coefficients[1] + t * (coefficients[2] + t * (coefficients[3] + t * coefficients[4]))));
    return 1 - sum * std::exp(-x * x);
}

int main()
{
    cout << "Testing for part A:" << endl;
    cout << "We will use delta and epsilon 0.001." << endl;
    double delta = 0.001;
    double epsilon = 0.001;

    // Defining the 4 test functions
    std::function<double(double)> f1 = [](double x) {return std::sqrt(x);};
    std::function<double(double)> f2 = [](double x) {return 1 / std::sqrt(x);};
    std::function<double(double)> f3 = [](double x) {return 4 * std::sqrt(1 - std::pow(x, 2));};
    std::function<double(double)> f4 = [](double x) {return log(x) / std::sqrt(x);};

    double start = 0.0;
    double end = 1.0;
    int count_1 = 0;
    int count_2 = 0;
    int count_3 = 0;
    int count_4 = 0;
    std::pair<double, double> ans_1 = Integration::recursiveAdaptiveIntegrator(f1, start, end, count_1, delta, epsilon);
    std::pair<double, double> ans_2 = Integration::recursiveAdaptiveIntegrator(f2, start, end, count_2, delta, epsilon);
    std::pair<double, double> ans_3 = Integration::recursiveAdaptiveIntegrator(f3, start, end, count_3, delta, epsilon);
    std::pair<double, double> ans_4 = Integration::recursiveAdaptiveIntegrator(f4, start, end, count_4, delta, epsilon);

    // Testing the four functions
    cout << "\nsqrt(x):" << endl;
    cout << "exact:                2/3" << endl;
    cout << "approximate:          " << ans_1.first << endl;
    cout << "accurate?             " << (std::abs(ans_1.first - 2.0/3.0) < delta && std::abs(ans_1.first - 2.0/3.0)/std::abs(ans_1.first) < epsilon) << endl;

    cout << "\n1 / sqrt(x):           " << endl;
    cout << "exact:                2" << endl;
    cout << "approximate:          " << ans_2.first << endl;
    cout << "accurate?             " << (std::abs(ans_2.first - 2.0) < delta && std::abs(ans_2.first - 2.0)/std::abs(ans_2.first) < epsilon) << endl;

    cout << "\n4 / sqrt(1 - x^2):     " << endl;
    cout << "exact:                pi" << endl;
    cout << "approximate:          " << ans_3.first << endl;
    cout << "accurate?             " << (std::abs(ans_3.first - M_PI) < delta && std::abs(ans_3.first - M_PI)/std::abs(ans_3.first) < epsilon) << endl;

    cout << "\nln(x) / sqrt(x):       " << endl;
    cout << "exact:                -4" << endl;
    cout << "approximate:          " << ans_4.first << endl;
    cout << "accurate?             " << (std::abs(ans_4.first + 4.0) < delta && std::abs(ans_4.first + 4.0)/std::abs(ans_4.first) < epsilon) << endl;

    // Define the range for our erf plot
    const double x_min_erf = -3.0;
    const double x_max_erf = 3.0;
    const int num_points_erf = 10000;

    // Vectors for storage
    Vector new_num(num_points_erf);
    Vector true_num(num_points_erf);

    // Open output file
    std::ofstream erf_data("erf.data");
    erf_data << "\"My function\"" << endl;

    // Generate data points using the new function and output to file
    for (int i = 0; i < num_points_erf; i++) 
    {
        double x = x_min_erf + (x_max_erf - x_min_erf) * i / (num_points_erf - 1);
        double y = Integration::errorFunction(x);
        new_num[i] = y;
        erf_data << x << " " << y << std::endl;
    }

    erf_data << "\n" << endl;
    erf_data << "\"Std function\"" << endl;

    // Generate data points using the std erf and output to file
    for (int i = 0; i < num_points_erf; i++) 
    {
        double x = x_min_erf + (x_max_erf - x_min_erf) * i / (num_points_erf - 1);
        double y = std::erf(x);
        true_num[i] = y;
        erf_data << x << " " << y << std::endl;
    }
    erf_data.close(); 

    // Calculate the average difference
    Vector diff(num_points_erf);
    for (int i = 0; i < num_points_erf; i++)
    {
        diff[i] = std::abs(new_num[i] - true_num[i]);
    }
    double average_error = diff.sum() / num_points_erf;
    double max_error = diff.max();

    cout << "\nTesting on error function    " << endl;
    cout << "Average error:                 " << average_error << endl;
    cout << "Max error:                     " << max_error << endl; 
    cout << "See Error_function.svg         " << endl;

    // Testing part B
    cout << "\nTesting for part B:" << endl;
    cout << "We will use delta and epsilon 0.001." << endl;

    // Defining the 2 test functions
    std::function<double(double)> f5 = [](double x) {return 1 / std::sqrt(x);};
    std::function<double(double)> f6 = [](double x) {return log(x) / std::sqrt(x);};

    // Use the clenshawCurtisTransformIntegrator
    int count_5 = 0;
    int count_6 = 0;
    std::pair<double, double> ans_5 = Integration::clenshawCurtisTransformIntegrator(f5, start, end, count_5, delta, epsilon);
    std::pair<double, double> ans_6 = Integration::clenshawCurtisTransformIntegrator(f6, start, end, count_6, delta, epsilon);

    cout << "\n1 / sqrt(x):           " << endl;
    cout << "exact:                2" << endl;
    cout << "approximate:          " << ans_5.first << endl;
    cout << "accurate?             " << (std::abs(ans_5.first - 2.0) < delta && std::abs(ans_5.first - 2.0)/std::abs(ans_5.first) < epsilon) << endl;
    cout << "old counter:          " << count_2 << endl;
    cout << "new counter:          " << count_5 << endl;
    cout << "Better?               " << (count_5 < count_2) << endl;

    cout << "\nln(x) / sqrt(x):       " << endl;
    cout << "exact:                -4" << endl;
    cout << "approximate:          " << ans_6.first << endl;
    cout << "accurate?             " << (std::abs(ans_6.first + 4.0) < delta && std::abs(ans_6.first + 4.0)/std::abs(ans_6.first) < epsilon) << endl;
    cout << "old counter:          " << count_4 << endl;
    cout << "new counter:          " << count_6 << endl;
    cout << "Better?               " << (count_6 < count_4) << endl;

    // Testing part C
    cout << "\nTesting for part C:" << endl;
    cout << "We will use delta and epsilon 0.001." << endl;
    
    // Defining two test cases
    std::function<double(double)> f7 = [](double x) {return std::exp(-x);};
    std::function<double(double)> f8 = [](double x) {return std::cos(x) * std::exp(-std::pow(x, 2));};

    // Use the clenshawCurtisTransformIntegrator with infinite limits
    double start_7 = 0;
    double end_7 = std::numeric_limits<double>::infinity();
    double start_8 = -std::numeric_limits<double>::infinity();
    double end_8 = std::numeric_limits<double>::infinity();
    int count_7 = 0;
    int count_8 = 0;
    std::pair<double, double> ans_7 = Integration::clenshawCurtisTransformIntegrator(f7, start_7, end_7, count_7, delta, epsilon);
    std::pair<double, double> ans_8 = Integration::clenshawCurtisTransformIntegrator(f8, start_8, end_8, count_8, delta, epsilon);

    cout << "\nexp(-x):             " << endl;
    cout << "exact:                1" << endl;
    cout << "approximate:          " << ans_7.first << endl;
    cout << "accurate?             " << (std::abs(ans_7.first - 1.0) < delta && std::abs(ans_7.first - 1.0)/std::abs(ans_7.first) < epsilon) << endl;
    cout << "count:                " << count_7 << endl;
    cout << "error estimate:       " << ans_7.second << endl;

    cout << "\ncos(x)*exp(-x^2)    " << endl;
    cout << "exact:                1.380" << endl;
    cout << "approximate:          " << ans_8.first << endl;
    cout << "accurate?             " << (std::abs(ans_8.first - 1.380) < delta && std::abs(ans_8.first - 1.380)/std::abs(ans_8.first) < epsilon) << endl;
    cout << "count:                " << count_8 << endl;
    cout << "error estimate:       " << ans_8.second << endl;

    cout << "\nWhile still approximately accurate, the use of infinite limits is reducing accuracy." << endl;

    cout << "\nPython results:" << endl;
    return 0;
}
