set terminal svg
set output "Error_function.svg"
set key top left
set xlabel "x"
set ylabel "y"
set title "Plot of the Error Functions"

# plot the error function
plot "erf.data" index 0 using 1:2 with lines title "my error function", \
     "erf.data" index 1 using 1:2 with lines title "std::erf"
