import numpy as np
from scipy.integrate import quad

def integrand1(x):
    return 1/np.sqrt(x)

def integrand2(x):
    return np.log(x)/np.sqrt(x)

def integrand3(x):
    return np.exp(-x)

def integrand4(x):
    return np.cos(x) * np.exp(-x**2)

result1, error1, info1 = quad(integrand1, 0, 1, epsabs=1e-3, epsrel=1e-3, full_output=1)
result2, error2, info2 = quad(integrand2, 0, 1, epsabs=1e-3, epsrel=1e-3, full_output=1)
result3, error3, info3 = quad(integrand3, 0, np.inf, epsabs=1e-3, epsrel=1e-3, full_output=1)
result4, error4, info4 = quad(integrand4, -np.inf, np.inf, epsabs=1e-3, epsrel=1e-3, full_output=1)

print(f"The integral of 1/sqrt(x) is {result1:.6f}, with {info1['neval']} integrand evaluations.")
print(f"The integral of ln(x)/sqrt(x) is {result2:.6f}, with {info2['neval']} integrand evaluations.")
print(f"The integral of exp(-x) is {result3:.6f}, with {info3['neval']} integrand evaluations and estimated error {error3:.6f}.")
print(f"The integral of cos(x)*exp(-x^2) is {result4:.6f}, with {info4['neval']} integrand evaluations and estimated error {error4:.6f}.")
