# Set the output file name and type
set terminal svg
set output "Fric_pendulum.svg"

# Set the title and axis labels
set title "Time Development of a Oscillator with Friction"
set xlabel "time"
set ylabel "theta, omega"

# Set grid lines
set grid xtics ytics lw 0.5 lc "black"
set xtics 2
set ytics 2
set xrange [-0.2:10.2]
set yrange [-4.2:4.2]


# Plot the two lines, one with error bars
plot "fric_pendulum.data" using 1:2 with lines lc "blue" title "theta", \
     "fric_pendulum.data" using 1:3 with lines lc "green" title "omega"