# Set the output file name and type
set terminal svg
set output "Planet_3.svg"

# Set the title and axis labels
set title "Precession of the Orbit of a Planet"
set xlabel "x"
set ylabel "y"

# Set square
set size ratio 1
set xrange [-2.4:2.4]
set yrange [-2.4:2.4]
set zeroaxis

# Plot the data
plot "planet_3.data" using (1 / $2) * cos($1) : (1 / $2) * sin($1) with lines notitle