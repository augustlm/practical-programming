# Set the output file name and type
set terminal svg
set output "Planet_2.svg"

# Set the title and axis labels
set title "Eliptical Orbit of a Planet"
set xlabel "x"
set ylabel "y"

# Set square
set size ratio 1
set xrange [-1.4:1.4]
set yrange [-1.4:2.4]
set zeroaxis

# Plot the data
plot "planet_2.data" using (1 / $2) * cos($1) : (1 / $2) * sin($1) with lines notitle