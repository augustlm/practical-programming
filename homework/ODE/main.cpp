#include<iostream>
#include<cmath>
#include<Vector_class.hpp>
#include<Generic_list.hpp>
#include<RungeKutta.hpp>

using std::cout, std::endl;

// Define the function f from u''=-u, which can be rewritten as a system of two first-order ODEs
Vector f(double x, Vector y)
{
    Vector result(2);
    result[0] = y[1];
    result[1] = -y[0];
    return result;
}

int main()
{
    // Testing for part A
    cout << "Testing for part A: " << endl;
    cout << "\nWe will solve u'' = -u with the initial conditions u(0) = 1 and u'(0) = 0." << endl;
    cout << "This should give cos(x) which we will then test at x = 10" << endl;

    // Set initial conditions
    double a = 0;
    Vector ya(2);
    ya[0] = 1;  // u(0) = 1
    ya[1] = 0;  // u'(0) = 0

    // Set integration bounds and tolerances
    double b = 10;
    double h = 0.01;
    double acc = 0.001;
    double eps = 0.001;

    // Solve the ODE using the RungeKutta class
    RungeKutta rk(f);
    Generic_list<double> x_list;
    Generic_list<Vector> y_list;
    Generic_list<double>* ptr_x_list = &x_list;
    Generic_list<Vector>* ptr_y_list = &y_list;
    std::pair<Generic_list<double>, Generic_list<Vector>> result = rk.driver(a, ya, b, h, acc, eps, ptr_x_list, ptr_y_list);

    // Output the solution vs analytical value
    cout << "Analytical cos(10):        " << std::cos(10) << endl;
    double predicted = result.second[result.second.get_size() - 1][0];
    cout << "Predicted cos(10):         " << predicted << endl;
    double abs_diff = std::abs(std::cos(10) - predicted);
    cout << "Absolute difference:       " << abs_diff << endl;
    double rel_diff = abs_diff / std::abs(std::cos(10));
    cout << "Relative difference:       " << rel_diff << endl;
    cout << "Desired absolute accuracy: " << acc << endl;
    cout << "Desired relative accuracy: " << eps << endl;
    cout << "Are they satisfied?        " << (abs_diff < acc || rel_diff < eps) << endl;
    cout << "See also Fric_pendulum.svg" << endl;

    // Testing for part B
    cout << "\nTesting for part B:" << endl;
    cout << "\nWe will redo the calculation done in part A without saving the lists before end." << endl;
    std::pair<Generic_list<double>, Generic_list<Vector>> test_pair = rk.driver(a, ya, b, h, acc, eps);
    // Output the solution vs analytical value
    cout << "Analytical cos(10):        " << std::cos(10) << endl;
    predicted = test_pair.second[0][0];
    cout << "Predicted cos(10):         " << predicted << endl;
    abs_diff = std::abs(std::cos(10) - predicted);
    cout << "Absolute difference:       " << abs_diff << endl;
    rel_diff = abs_diff / std::abs(std::cos(10));
    cout << "Relative difference:       " << rel_diff << endl;
    cout << "Desired absolute accuracy: " << acc << endl;
    cout << "Desired relative accuracy: " << eps << endl;
    cout << "Are they satisfied?        " << (abs_diff < acc || rel_diff < eps) << endl;
    cout << "Length of x and y lists:   " << test_pair.first.get_size() << endl;
    cout << "See also all Planet_x.svg  " << endl;

    // Testing for part C
    cout << "\nThe result for part C can be found in Three_body.gif." << endl;

    return 0;
}
