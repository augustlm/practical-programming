#include<iostream>
#include<cmath>
#include<fstream>
#include<Vector_class.hpp>
#include<Generic_list.hpp>
#include<RungeKutta.hpp>

using std::cout, std::endl;

// Define the function to integrate
Vector f(double t, Vector y)
{
    // Make some position vectors so it's clean code
    Vector body_1(2);
    Vector body_2(2);
    Vector body_3(2);
    body_1[0] = y[0];
    body_1[1] = y[1];
    body_2[0] = y[2];
    body_2[1] = y[3];
    body_3[0] = y[4];
    body_3[1] = y[5];

    // First part is updating position
    Vector res(12);
    res[0] = y[6];
    res[1] = y[7];
    res[2] = y[8];
    res[3] = y[9];
    res[4] = y[10];
    res[5] = y[11];

    // Update velocity
    Vector diff_12 = body_2 - body_1;
    Vector diff_13 = body_3 - body_1;
    Vector diff_21 = body_1 - body_2;
    Vector diff_23 = body_3 - body_2;
    Vector diff_31 = body_1 - body_3;
    Vector diff_32 = body_2 - body_3; 
    
    Vector acc_1 = (diff_12 / std::pow(diff_12.norm(), 3)) - (diff_13 / std::pow(diff_13.norm(), 3)) * -1.0;
    Vector acc_2 = (diff_21 / std::pow(diff_21.norm(), 3)) - (diff_23 / std::pow(diff_23.norm(), 3)) * -1.0;
    Vector acc_3 = (diff_31 / std::pow(diff_31.norm(), 3)) - (diff_32 / std::pow(diff_32.norm(), 3)) * -1.0;

    res[6] = acc_1[0];
    res[7] = acc_1[1];
    res[8] = acc_2[0];
    res[9] = acc_2[1];
    res[10] = acc_3[0];
    res[11] = acc_3[1];    
    return res;
}

int main()
{
    // Set initial conditions
    Vector ya(12);
    ya[0] = 0.97000436;
    ya[1] = -0.24308753;
    ya[2] = -0.97000436;
    ya[3] = 0.24308753;
    ya[4] = 0;
    ya[5] = 0;
    ya[6] = 0.93240737/2.0;
    ya[7] = 0.86473146/2.0;
    ya[8] = 0.93240737/2.0;
    ya[9] = 0.86473146/2.0;
    ya[10] = -0.93240737;
    ya[11] = -0.86473146;
    

    // Set integration bounds and tolerances
    double a = 0;
    double b = 6.32591398;
    double h = 0.01;
    double acc = 0.0001;
    double eps = 0.0001;

    // Solve the ODE using the RungeKutta class
    RungeKutta rk(f);
    Generic_list<double> x_list;
    Generic_list<Vector> y_list;
    Generic_list<double>* ptr_x_list = &x_list;
    Generic_list<Vector>* ptr_y_list = &y_list;
    std::pair<Generic_list<double>, Generic_list<Vector>> result = rk.driver(a, ya, b, h, acc, eps, ptr_x_list, ptr_y_list);

    // Generating output file
    for (int i = 0; i < result.first.get_size(); i++)
    {
        cout << result.first[i] << "     " << result.second[i][0] << "     " << result.second[i][1] << "     " << result.second[i][2] << "     " << result.second[i][3] << "     " << result.second[i][4] << "     " << result.second[i][5] << endl;
        cout << "\n" << endl; 
    }
    return 0;
}
