# Set the output file name and type
set terminal gif animate
set output "Three_body.gif"

# Set the title and axis labels
set title "Animation of a Stable Solution to the Three Body Problem"
set xlabel "x"
set ylabel "y"

# Set square
set xrange [-1.5:1.5]
set yrange [-0.4:0.4]
set zeroaxis

stats 'three_body.data' using 2 prefix "A" nooutput

# Plot the data
do for [i = 1:int(A_blocks):50] {
    plot "three_body.data" index (i-1) using 2:3 with points pt 7 ps 4 title "Body 1", \
        "three_body.data" index (i-1) using 4:5 with points pt 7 ps 4 title "Body 2", \
        "three_body.data" index (i-1) using 6:7 with points pt 7 ps 4 title "Body 3"
}