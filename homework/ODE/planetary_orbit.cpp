#include<iostream>
#include<cmath>
#include<fstream>
#include<Vector_class.hpp>
#include<Generic_list.hpp>
#include<RungeKutta.hpp>

using std::cout, std::endl;

// Define the function to integrate
Vector f_1(double phi, Vector y)
{
    double epsilon = 0;
    Vector res(2);
    double u = y[0];
    res[0] = y[1];
    res[1] = 1.0 - u + epsilon * u * u;
    return res;
}

// Define the function to integrate
Vector f_2(double phi, Vector y)
{
    double epsilon = 0.01;
    Vector res(2);
    double u = y[0];
    res[0] = y[1];
    res[1] = 1.0 - u + epsilon * u * u;
    return res;
}

int main()
{
    // Set initial conditions
    double a = 0;
    Vector ya(2);
    ya[0] = 0.99;
    ya[1] = 0.0;

    // Set integration bounds and tolerances
    double b = 10;
    double h = 0.01;
    double acc = 0.001;
    double eps = 0.001;

    // Solve the ODE using the RungeKutta class
    RungeKutta rk_1(f_1);
    Generic_list<double> x_list_1;
    Generic_list<Vector> y_list_1;
    Generic_list<double>* ptr_x_list_1 = &x_list_1;
    Generic_list<Vector>* ptr_y_list_1 = &y_list_1;
    std::pair<Generic_list<double>, Generic_list<Vector>> result_1 = rk_1.driver(a, ya, b, h, acc, eps, ptr_x_list_1, ptr_y_list_1);

    // Output to data file 1
    std::ofstream planet_1;
    planet_1.open("planet_1.data");
    for (int i = 0; i < result_1.first.get_size(); i++)
    {
        planet_1 << result_1.first[i] << "     " << result_1.second[i][0] << "     " << result_1.second[i][1] << endl;
    }
    planet_1.close();

    // Set integration bounds and tolerances
    b = 50;
    h = 0.01;
    acc = 0.01;
    eps = 0.01;

    // New start conditions
    ya[1] = -0.5;

    // Solve the ODE using the RungeKutta class
    Generic_list<double> x_list_2;
    Generic_list<Vector> y_list_2;
    Generic_list<double>* ptr_x_list_2 = &x_list_2;
    Generic_list<Vector>* ptr_y_list_2 = &y_list_2;
    std::pair<Generic_list<double>, Generic_list<Vector>> result_2 = rk_1.driver(a, ya, b, h, acc, eps, ptr_x_list_2, ptr_y_list_2);

    // Output to data file 2
    std::ofstream planet_2;
    planet_2.open("planet_2.data");
    for (int i = 0; i < result_2.first.get_size(); i++)
    {
        planet_2 << result_2.first[i] << "     " << result_2.second[i][0] << "     " << result_2.second[i][1] << endl;
    }
    planet_2.close();

    // Set integration bounds and tolerances
    b = 50;
    h = 0.01;
    acc = 0.01;
    eps = 0.01;

    // Solve the ODE using the RungeKutta class
    RungeKutta rk_2(f_2);
    Generic_list<double> x_list_3;
    Generic_list<Vector> y_list_3;
    Generic_list<double>* ptr_x_list_3 = &x_list_3;
    Generic_list<Vector>* ptr_y_list_3 = &y_list_3;
    std::pair<Generic_list<double>, Generic_list<Vector>> result_3 = rk_2.driver(a, ya, b, h, acc, eps, ptr_x_list_3, ptr_y_list_3);

    // Output to data file 2
    std::ofstream planet_3;
    planet_3.open("planet_3.data");
    for (int i = 0; i < result_3.first.get_size(); i++)
    {
        planet_3 << result_3.first[i] << "     " << result_3.second[i][0] << "     " << result_3.second[i][1] << endl;
    }
    planet_3.close();

    return 0;
}
