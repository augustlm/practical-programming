#include<iostream>
#include<cmath>
#include<Vector_class.hpp>
#include<Generic_list.hpp>
#include<RungeKutta.hpp>

using std::cout, std::endl;

// Define the function f for the pendulum
Vector pendulum(double t, Vector y)
{
    double b = 0.25;
    double c = 5.0;
    Vector result(2);
    result[0] = y[1];
    result[1] = -b * y[1] - c * std::sin(y[0]);
    return result;
}

int main()
{
    // Set initial conditions
    double a = 0;
    Vector ya(2);
    ya[0] = M_PI - 0.1;
    ya[1] = 0;

    // Set integration bounds and tolerances
    double b = 10;
    double h = 0.01;
    double acc = 0.001;
    double eps = 0.001;

    // Solve the ODE using the RungeKutta class
    RungeKutta rk(pendulum);
    Generic_list<double> x_list;
    Generic_list<Vector> y_list;
    Generic_list<double>* ptr_x_list = &x_list;
    Generic_list<Vector>* ptr_y_list = &y_list;
    std::pair<Generic_list<double>, Generic_list<Vector>> result = rk.driver(a, ya, b, h, acc, eps, ptr_x_list, ptr_y_list);

    // Generating output file
    for (int i = 0; i < result.first.get_size(); i++)
    {
        cout << result.first[i] << "     " << result.second[i][0] << "     " << result.second[i][1] << endl;
    }

    return 0;
}
