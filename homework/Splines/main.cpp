#include<iostream>
#include"Vector_class.hpp"
#include"Qspline.hpp"

using std::cout, std::endl;

int main()
{
    cout << "Testing the functions implemented in part A:" << endl;
    cout << "\nBinomial search: " << endl;
    Vector x_axis = Vector::axisVector(0, 10);
    Vector y_values = Vector::randomVectorInt(10, 1, 100);
    cout << "x_axis:   " << x_axis;
    cout << "y_values: " << y_values;
    int lower_idx = Qspline::binomialSearch(x_axis, 6.7);
    cout << "Finding the axis point below x = 6.7: " << x_axis[lower_idx] << endl;
    cout << "\nLinear Interpolation: " << endl;
    cout << "x_axis:   " << x_axis;
    cout << "y_values: " << y_values;
    cout << "Estimating y at x = 6.7 using linear interpolation between " << y_values[lower_idx] << " and " << y_values[lower_idx + 1] << ": " << Qspline::linearInterpolation(x_axis, y_values, 6.7) << endl;
    cout << "\nLinear Spline Integral: " << endl;
    cout << "x_axis:   " << x_axis;
    cout << "y_values: " << y_values;
    cout << "Estimating the integral of the linear splines from 0 to 6.7: " << Qspline::linearSplineIntegral(x_axis, y_values, 6.7) << endl;
    cout << "\nTesting the functions implemented in part B:" << endl;
    cout << "\nUsing the table {x_i = i, y_i = 1} for i = 1, ..., 5: " << endl;
    Vector test_x = Vector::axisVector(1, 6);
    Vector test_y_1 = Vector::vectorWithOneValue(5, 1);
    Qspline test_1(test_x, test_y_1);
    Vector b_1 = Vector::vectorWithOneValue(4, 0);
    Vector c_1 = Vector::vectorWithOneValue(4, 0);
    cout << "Qspline b:         " << test_1.getB();
    cout << "Hand calculated b: " << b_1;
    cout << "Qspline c:         " << test_1.getC();
    cout << "Hand calculated c: " << c_1;
    cout << "\nUsing the table {x_i = i, y_i = i} for i = 1, ..., 5: " << endl;
    Vector test_y_2(test_x);
    Qspline test_2(test_x, test_y_2);
    Vector b_2 = Vector::vectorWithOneValue(4, 1);
    Vector c_2 = Vector::vectorWithOneValue(4, 0);
    cout << "Qspline b:         " << test_2.getB();
    cout << "Hand calculated b: " << b_2;
    cout << "Qspline c:         " << test_2.getC();
    cout << "Hand calculated c: " << c_2;
    cout << "\nUsing the table {x_i = i, y_i = i^2} for i = 1, ..., 5: " << endl;
    Vector test_y_3(test_x);
    for (int i = 0; i < test_y_3.getSize(); i++)
    {
        test_y_3[i] *= test_y_3[i];
    }
    Qspline test_3(test_x, test_y_3);
    Vector b_3 = Vector::axisVector(2, 9, 2);
    Vector c_3 = Vector::vectorWithOneValue(4, 1);
    cout << "Qspline b:         " << test_3.getB();
    cout << "Hand calculated b: " << b_3;
    cout << "Qspline c:         " << test_3.getC();
    cout << "Hand calculated c: " << c_3;
    return 0;
}