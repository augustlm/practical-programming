# Set the output file name and type
set terminal svg
set output "Lin_test.svg"

# Set the title and axis labels
set title "Linear Spline Interpolation Demonstrated"
set xlabel "x"
set ylabel "y"
set xrange [0:9]
set yrange [0:20]



# Do the plotting
plot "lin_test.data" index 0 using 1:2 with points pt 7 ps 0.5 lc "black" title "Given Points", \
     "lin_test.data" index 0 using 1:2 with lines lc "black" title "Liner Splines", \
     "lin_test.data" index 1 using 1:2 with points pt 2 ps 1 lc "red" title "Interpolated Values"