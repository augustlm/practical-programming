#include<iostream>
#include"Vector_class.hpp"
#include"Qspline.hpp"

using std::cout, std::endl;

int main()
{
    Vector x_axis = Vector::axisVector(0, 10);
    Vector y_values = Vector::randomVectorInt(10, 1, 20);
    for (int i = 0; i < x_axis.getSize(); i++)
    {
        cout << x_axis[i] << "     " << y_values[i] << endl;
    }
    cout << "\n" << endl;
    for (int i = 0; i < 9; i++)
    {
        double mid = x_axis[i] + 0.5;
        cout << mid << "     " << Qspline::linearInterpolation(x_axis, y_values, mid) << endl;
    }
    
    return 0;
}