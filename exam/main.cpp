#include<iostream>
#include<functional>
#include<cmath>
#include<fstream>
#include<limits>
#include<Integration.hpp>
#include<Vector_class.hpp>

using std::cout, std::endl;

int main()
{
    cout << "Testing the 2D integrator:" << endl;
    cout << "Three tests will be performed. All make use of the Clenshaw-Curtis transformation modified to handle infinite integration bounds." << endl;
    cout << "We will use delta and epsilon 0.001." << endl;
    double delta = 0.001;
    double epsilon = 0.001;
    std::function<double(double, double)> f1 = [](double x, double y) {return 1.0 / std::sqrt(x + y);};
    std::function<double(double)> d1 = [](double x) {return 0;};
    std::function<double(double)> u1 = [](double x) {return 2 * x;};
    std::function<double(double, double)> f2 = [](double x, double y) {return std::exp(-std::pow(x, 2) - std::pow(y, 2));};
    std::function<double(double)> d2 = [](double x) {return -std::numeric_limits<double>::infinity();};
    std::function<double(double)> u2 = [](double x) {return std::numeric_limits<double>::infinity();};
    std::function<double(double, double)> f3 = [](double x, double y) {return std::exp(-y) * (std::pow(x, 4) + std::pow(y, 4));};
    std::function<double(double)> d3 = [](double x) {return 2 * std::pow(x, 2) - x + 5;};
    std::function<double(double)> u3 = [](double x) {return std::numeric_limits<double>::infinity();};
    double a = 0.0;
    double b = 1.0;
    int count_1 = 0;
    std::pair<double, double> ans_1 = Integration::twoDIntegration(f1, a, b, d1, u1, epsilon, delta, count_1);
    a = -std::numeric_limits<double>::infinity();
    b = std::numeric_limits<double>::infinity();
    int count_2 = 0;
    std::pair<double, double> ans_2 = Integration::twoDIntegration(f2, a, b, d2, u2, epsilon, delta, count_2);
    a = -5;
    b = 10;
    int count_3 = 0;
    std::pair<double, double> ans_3 = Integration::twoDIntegration(f3, a, b, d3, u3, epsilon, delta, count_3);
    cout << "\nint [0, 1] int[0, 2*x] 1 / sqrt(x + y) dy dx:" << endl;
    cout << "exact:           0.976" << endl;
    cout << "approximate:     " << ans_1.first << endl;
    cout << "accurate?        " << (std::abs(ans_1.first - 0.976) < delta && std::abs(ans_1.first - 0.976)/std::abs(ans_1.first) < epsilon) << endl;
    cout << "iterations:      " << count_1 << endl;
    cout << "estimated error: " << ans_1.second << endl;
    cout << "\nint [-inf, inf] int[-inf, inf] exp(-x^2 - y^2) dy dx:" << endl;
    cout << "exact:           3.141" << endl;
    cout << "approximate:     " << ans_2.first << endl;
    cout << "accurate?        " << (std::abs(ans_2.first - 3.141) < delta && std::abs(ans_2.first - 3.141)/std::abs(ans_2.first) < epsilon) << endl;
    cout << "iterations:      " << count_2 << endl;
    cout << "estimated error: " << ans_2.second << endl;
    cout << "\nint [-5, 10] int[-inf, 2x^2 - x + 5] exp(-y) * (x^4 + y ^4) dy dx:" << endl;
    cout << "exact:           20.191" << endl;
    cout << "approximate:     " << ans_3.first << endl;
    cout << "accurate?        " << (std::abs(ans_3.first - 20.191) < delta && std::abs(ans_3.first - 20.191)/std::abs(ans_3.first) < epsilon) << endl;
    cout << "iterations:      " << count_3 << endl;
    cout << "estimated error: " << ans_3.second << endl;
    cout << "\nWhen given a very challenging integral, the accuracy suffers slightly." << endl;
    cout << "Note when comparing to python, the third integral cannot be evaluated using the dblquad method." << endl;
    cout << "\nPython results:" << endl;
}