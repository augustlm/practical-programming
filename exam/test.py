import numpy as np
from scipy.integrate import dblquad

def integrand1(x, y):
    return 1/np.sqrt(x + y)

def integrand2(x, y):
    return np.exp(-x**2 - y**2)

def integrand3(x, y):
    return np.exp(-y) * (x**4 + y**4)

result1, error1 = dblquad(integrand1, 0, 1, lambda x: 0, lambda x: 2*x, epsabs=1e-3, epsrel=1e-3)
result2, error2 = dblquad(integrand2, -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf, epsabs=1e-3, epsrel=1e-3)
result3, error3 = dblquad(integrand3, -5, 10, lambda x: -np.inf, lambda x: 2*x**2 - x + 5, epsabs=1e-3, epsrel=1e-3, )

print(f"The integral of 1/sqrt(x + y) is {result1:.6f} with error of {error1:.6f}.")
print(f"The integral of exp(-x^2 - y^2) is {result2:.6f} with error of {error2:.6f}.")
print(f"The integral of exp(-y) * (x^4 + y^4) is {result3:.6f} with error of {error3:.6f}.")