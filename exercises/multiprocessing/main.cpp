#include<iostream>
#include<vector>
#include<thread>

class data 
{
public:

    // Information required for harmonic sum
    int range_start;
    int range_end;
    double sum;

};

// Function which uses the data in a data class to calculate the harmonic sum, which is kept. 
void harmonic(data& some_data) 
{
    some_data.sum = 0;
    for (int i = some_data.range_start; i < some_data.range_end; i++) 
    {
        some_data.sum += 1.0 / i;
    }
}


int main(int argc, char** argv) 
{
    // Sanity check that usage is correct
    if (argc != 3) 
    {
        std::cout << "Usage: " << argv[0] << " <num_terms> <num_threads>" << std::endl;
        return 1;
    }

    // Assign number of threads
    const int num_terms = std::stoi(argv[1]);
    const int num_threads = std::stoi(argv[2]);

    // Splits the terms to be calculated up based on the number of threads
    std::vector<data> data_list(num_threads);
    const int terms_per_thread = num_terms / num_threads;

    // Initialises the threads
    std::vector<std::thread> threads(num_threads);

    // This loop goes through all threads and starts a calculation with some terms on it
    for (int i = 0; i < num_threads; i++) 
    {
        data_list[i].range_start = i * terms_per_thread + 1;
        data_list[i].range_end   = (i + 1) * terms_per_thread + 1;
        threads[i] = std::thread(harmonic, std::ref(data_list[i])); // ref required so data is changed, not a copy
    }

    // Join the threads together again
    for (std::thread& thread : threads) 
    {
        thread.join();
    }

    // Calculate the sum of the differenet sums and output
    double sum = 0;
    for (data& this_data : data_list) 
    {
        sum += this_data.sum;
    }
    std::cout << "Harmonic sum of " << num_terms << " terms: " << sum << std::endl;

    // Good run returns 0
    return 0;
}
