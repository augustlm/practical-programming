#include<iostream>
#include<omp.h>

int main()
{
    // Init values
    const int N = 1000000000;
    double sum = 0;

    // preprocessor information to parallise sum
    #pragma omp parallel for reduction(+:sum) 
    // for loop to calculate sum
    for (int i = 1; i <= N; i++)
    {
        sum += (double)1 / (double)i;
    }

    // print output
    std::cout << "Harmonic sum of " << N << " terms: " << sum << "\n";

    // good run returns 0
    return 0;
}