#pragma once

template <typename T> class generic_list
{
private:
    
    // Data values
    int l_size;
    T* ptr_to_data;

public:

    // Constructors
    generic_list();
    generic_list(int size);
    generic_list(const generic_list& other);

    // Destructor
    ~generic_list();

    // Public member functions
    const int get_size() const;
    void add(T item);

    // Operator
    T& operator[](const int i);
    const T& operator[](const int i) const;
};

// Implementations

// Constructors

// Default
template <typename T> generic_list<T>::generic_list()
    :   l_size (0)
    ,   ptr_to_data (nullptr)
{
}

// Standard
template <typename T> generic_list<T>::generic_list(int size)
    :   l_size (size)
{
    ptr_to_data = new T[size];
}

// Copy
template <typename T> generic_list<T>::generic_list(const generic_list& other)
    :   l_size (other.l_size)
{
    ptr_to_data = new T[other.l_size];
    for (int i = 0; i < other.l_size; i++)
    {
        ptr_to_data[i] = other[i];
    }
    
}

// Destructors

// Standard
template <typename T> generic_list<T>::~generic_list()
{
    if (ptr_to_data != nullptr)
    {
        delete[] ptr_to_data;
        ptr_to_data = nullptr;
    }
    
}

// Public member functions

// Get_size returns the size of the list
template <typename T>
const int generic_list<T>::get_size() const
{
    return this->l_size;
}

// Add
template <typename T>
void generic_list<T>::add(T item)
{
    T* new_ptr_to_data = static_cast<T*>(realloc(ptr_to_data, (l_size + 1) * sizeof(T)));
    if (new_ptr_to_data == nullptr)
    {
        throw std::bad_alloc();
    }
    ptr_to_data = new_ptr_to_data;
    ptr_to_data[l_size] = item;
    l_size += 1;
}


// Operators

// Access operator [] non const
template <typename T>
T& generic_list<T>::operator[](const int i)
{
    if (i<0 || i >= l_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return ptr_to_data[i];
}

// Access operator [] const
template <typename T>
const T& generic_list<T>::operator[](const int i) const
{
    if (i<0 || i >= l_size)
    {
        throw std::out_of_range("Index out of range");
    }
    return ptr_to_data[i];
}
