#include<iostream>
#include<fstream>
#include<string>
#include"generic_list.hpp"

int main()
{
    // Open input file and init list
    std::ifstream infile("input.txt");
    generic_list<double> mylist;

    // Put numbers into file and close stream
    double number;
    while (infile >> number)
    {
        mylist.add(number);
    }
    infile.close();

    // Output elements into std output with specified precision
    std::cout.precision(2);
    for (int i = 0; i < mylist.get_size(); i++)
    {
        std::cout << std::scientific << mylist[i] << "\n";
    }
    
    // Good run returns 0
    return 0;
}