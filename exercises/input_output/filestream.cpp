#include<iostream>
#include<cstring>
#include<cctype>
#include<cmath>
#include<fstream>

int main(int nargs, char *vargs[])
{
    std::string input_file_name;
    std::string output_file_name;
    // Read keywords
    for (int i = 0; i < nargs; i++)
    {
        std::string input = vargs[i];
        std::string keyword = input.substr(0, input.find(":"));
        if (keyword == "-input")
        {
            input_file_name = input.substr(input.find(":") + 1, input.length() - input.find(":"));
        }
        else if (keyword == "-output")
        {
            output_file_name = input.substr(input.find(":") + 1, input.length() - input.find(":"));
        }
    }
    std::ifstream infile(input_file_name);
    std::ofstream out_file(output_file_name, std::ios::app);
    int number;
    while (infile >> number)
    {
        out_file << number << " " << sin(number * M_PI / 180) << " " << cos(number * M_PI / 180) << "\n";
    } 
    out_file.close();
    return 0;
}