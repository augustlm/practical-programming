#include<iostream>
#include<cstring>
#include<cctype>
#include<cmath>

int main()
{
    std::string input;
    getline(std::cin, input, static_cast<char>(EOF));
    char delim_new_line = '\n';
    char delim_space = ' ';
    char delim_tab = '\t';
    int sub_string_len = 1;
    bool found = false;
    for (int start = 0; start < input.length(); start++)
    {
        // make sure not making substring out of delims. When new delim hit set found to false.
        if (input[start] == delim_new_line || input[start] == delim_space || input[start] == delim_tab)
        {
            found = false;
            continue;
        }

        // check if we have hit delim and then make substr. Else make step longer. Only runs till hit
        while (!found)
        {
            if (input[start + sub_string_len] == delim_new_line || input[start + sub_string_len] == delim_space || input[start + sub_string_len] == delim_tab)
            {
                int number = std::stoi(input.substr(start, sub_string_len));
                std::cout << number << " " << sin(number * M_PI / 180) << " " << cos(number * M_PI / 180) << std::endl;
                sub_string_len = 1;
                found = true;
            }
            else
            {
                sub_string_len += 1;
            } 
        }
    }
    return 0;
}