#include<iostream>
#include<cstring>
#include<cctype>
#include<cmath>

int main(int nargs, char *vargs[])
{
    std::string numbers = vargs[1];
    for (int i = 0; i < numbers.length(); i++)
    {
        if (isdigit(numbers[i]))
        {
            int number = std::stoi(numbers.substr(i, 1));
            std::cout << number << " " << sin(number * M_PI / 180) << " " << cos(number * M_PI / 180) << std::endl;
        }
    }
}