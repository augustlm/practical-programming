#include<iostream>
#include<cmath>
#include<math.h>
#include"sfuns.hpp"

using namespace std;

int main()
{
    cout << "The square root of 2 is " << sqrt(2.0) << endl;
    cout << "Test the answer squared " << sqrt(2.0) * sqrt(2.0) << endl;
    cout << "2 to the power of 1/5 is " << pow(2.0, 0.2) << endl;
    cout << "e to the power of pi is " << exp(M_PI) << endl;
    cout << "pi to the power of e is " << pow(M_PI, exp(1.0)) << endl;
    cout << "The gamma function of 1 is " << my_gamma(1.0) <<endl;
    cout << "The gamma function of 2 is " << my_gamma(2.0) <<endl;
    cout << "The gamma function of 3 is " << my_gamma(3.0) <<endl;
    cout << "The gamma function of 10 is " << my_gamma(10.0) <<endl;
    return 0;
}
