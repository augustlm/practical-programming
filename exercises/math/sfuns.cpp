#include<iostream>
#include<cmath>
#include<math.h>
#include"sfuns.hpp"

using namespace std;

double my_gamma(double num)
{
    double ans;
    if (num < 0.0)
    {
        ans = M_PI / (sin(M_PI * num) * my_gamma(1.0 - num));
        return ans;
    }
    else if (num < 9.0)
    {
        ans = my_gamma(num + 1) / num;
        return ans;
    }
    else
    {
        ans = num * log(num + 1 / (12 * num - 1 / num / 10)) - num + log(2 * M_PI / num) / 2;
        return exp(ans);
    }
}
