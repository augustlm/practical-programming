#pragma once

double error_function(const double x);
double gamma_function(const double x);
double ln_gamma_function(const double x);