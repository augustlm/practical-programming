#include<cmath>
#include<stdexcept>
#include"math.h"

double gamma_function(const double x) 
{
    // single precision gamma function (Gergo Nemes, from Wikipedia)
    if (x < 0) 
    {
        return M_PI / sin(M_PI * x) / gamma_function(1 - x);
    }
    if (x < 9) 
    {
        return gamma_function(x + 1) / x;
    }
    double lngamma = x * log(x + 1 / (12 * x - 1 / x / 10)) - x + log(2 * M_PI / x) / 2;
    return exp(lngamma);
}

double ln_gamma_function(const double x) 
{
    if (x <= 0) 
    {
        throw std::invalid_argument("ln_gamma_function: x <= 0");
    }

    if (x < 9) 
    {
        return ln_gamma_function(x + 1) - log(x);
    }

    double lngamma = x * log(x + 1.0 / (12.0 * x - 1.0 / (10.0 * x))) - x + log(2.0 * M_PI / x) / 2.0;
    return lngamma;
}
