set terminal svg
set output "error_function.svg"
set key top left
set xlabel "x"
set ylabel "y"
set tics out
set title "Plot of my Error Function vs Known Points"

# plot the error function
plot "error_function.data" using 1:2 with lines title "my error function", \
     "known_erf.data" using 1:2  with points pt "x" lc "red" title "known points"