#include<iostream>
#include<fstream>
#include"functions.hpp"

int main()
{

    // Define the range for our erf plot
    const double x_min_erf = -3.0;
    const double x_max_erf = 3.0;
    const int num_points_erf = 10000;

    // Open output file
    std::ofstream outfile_1("error_function.data");

     // Generate data points and output to file
    for (int i = 0; i < num_points_erf; i++) 
    {
        double x = x_min_erf + (x_max_erf - x_min_erf) * i / (num_points_erf - 1);
        double y = error_function(x);
        outfile_1 << x << " " << y << std::endl;
    }

    // Close the output file
    outfile_1.close();

    // Define the range for our gamma plot
    const double x_min_gamma = -5.0;
    const double x_max_gamma = 5.0;
    const int num_points_gamma = 10000;

    // Open output file
    std::ofstream outfile_2("gamma_function.data");

     // Generate data points and output to file
    for (int i = 0; i < num_points_gamma; i++) 
    {
        double x = x_min_gamma + (x_max_gamma - x_min_gamma) * i / (num_points_gamma - 1);
        double y = gamma_function(x);
        outfile_2 << x << " " << y << std::endl;
    }

    // Close the output file
    outfile_2.close();

    // Define the range for our ln gamma plot
    const double x_min_ln_gamma = 0.01;
    const double x_max_ln_gamma = 5.0;
    const int num_points_ln_gamma = 5000;

    // Open output file
    std::ofstream outfile_3("ln_gamma_function.data");

     // Generate data points and output to file
    for (int i = 0; i < num_points_ln_gamma; i++) 
    {
        double x = x_min_ln_gamma + (x_max_ln_gamma - x_min_ln_gamma) * i / (num_points_ln_gamma - 1);
        double y = ln_gamma_function(x);
        outfile_3 << x << " " << y << std::endl;
    }

    // Close the output file
    outfile_3.close();

    return 0;
}