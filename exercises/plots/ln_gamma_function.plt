# make a pretty plot
set terminal svg
set output "ln_gamma_function.svg"
set key top right
set xrange [0:5]
set yrange [-1:5]
set zeroaxis
set border linetype 1 linewidth 1 lc "black"
set zeroaxis linetype 1 linewidth 1 lc "black"
set xtics axis
set ytics axis
set title "Ln of Gamma Function"

# plot the error function
plot "ln_gamma_function.data" using 1:2 with lines lc "red" title "my ln gamma function", \
     "known_factorials.data" using 1:(log($2)) with points pt "x" lc "blue" title "known factorials"