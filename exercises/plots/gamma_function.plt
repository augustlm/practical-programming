# make a pretty plot
set terminal svg
set output "gamma_function.svg"
set key bottom right
set xrange [-5:5]
set yrange [-5:5]
set zeroaxis
set border linetype 1 linewidth 1 lc "black"
set zeroaxis linetype 1 linewidth 1 lc "black"
set xtics axis
set ytics axis
set title "Gamma Function"

# Set some blue lines
set arrow from 0,graph 0 to 0,graph 1 nohead dt 2 lc "blue"
set arrow from -1,graph 0 to -1,graph 1 nohead dt 2 lc "blue"
set arrow from -2,graph 0 to -2,graph 1 nohead dt 2 lc "blue"
set arrow from -3,graph 0 to -3,graph 1 nohead dt 2 lc "blue"
set arrow from -4,graph 0 to -4,graph 1 nohead dt 2 lc "blue"

# plot the error function
plot "gamma_function.data" using 1:2 with lines lc "red" title "my gamma function", \
     "known_factorials.data" using 1:2  with points pt "x" lc "blue" title "known factorials"