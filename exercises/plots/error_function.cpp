#include <cmath>
#include <vector>

double error_function(const double x) 
{
    if (x < 0) 
    {
        return -error_function(-x);
    }
    std::vector<double> coeffecients{0.254829592, -0.284496736, 1.421413741, -1.453152027, 1.061405429};
    double t = 1 / (1 + 0.3275911 * x);
    double sum = t * (coeffecients[0] + t * (coeffecients[1] + t * (coeffecients[2] + t * (coeffecients[3] + t * coeffecients[4]))));
    return 1 - sum * std::exp(-x * x);
}