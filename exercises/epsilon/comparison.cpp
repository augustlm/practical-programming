#include<iostream>
#include<iomanip>
#include<cmath>

using namespace std;

// Own comp function which takes floating point accuracy into account
bool approx(double a, double b, double acc=1e-9, double eps=1e-9)
{
    if (abs(a - b) < acc)
    {
        return true;
    }
    else if (abs(a - b) / max(abs(a), abs(b)) < eps)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}

int main()
{
    // First test using ==
    double d1 = 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1;
    double d2 = (double)8 * 0.1;
    cout << "Testing using built in ==: d1 = " << fixed << setprecision(15) << d1 << ", d2 = " << d2 << ", d1==d2 returns " << (d1 == d2) << endl;

    // Second test using own function
    cout << "Testing own approx function: d1 = " << fixed << setprecision(15) << d1 << ", d2 = " << d2 << ", approx(d1, d2) returns " << approx(d1, d2) << endl;
}