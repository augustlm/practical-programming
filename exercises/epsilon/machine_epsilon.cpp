#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    // Double section
    double my_double = 1;
    while ((double)1 + my_double != (double)1)
    {
        my_double = my_double / 2;
    }
    my_double = my_double * 2;

    // Float section
    float my_float = 1;
    while ((float)1 + my_float != (float)1)
    {
        my_float = my_float / 2;
    }
    my_float = my_float * 2;

    // Output section
    cout << "Machine epsilon for a double: " << my_double << " vs 2**(-52): " << pow(2, -52) << endl;
    cout << "Machine epsilon for a float: " << my_float << " vs 2**(-23): " << pow(2, -23) << endl;
    return 0;
}