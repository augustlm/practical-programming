#include<iostream>
#include<cmath>

using namespace std;

int main()
{
    // Initialise values
    int large_sum = (int)1e6;
    double epsilon = pow(2,-52);
    double tiny = epsilon / 2;
    double sumA = 0, sumB = 0;

    // Make sum A
    sumA += (double)1; 
    for(int i = 0; i < large_sum; i++)
    {
        sumA += tiny;
    }

    // Make sum B
    for(int i = 0; i < large_sum; i++)
    {
        sumB += tiny;
    } 
    sumB += (double)1;

    // Write output
    cout << "Value of sum A - 1: " << sumA - 1 << " should be the same as n * tiny: " << large_sum * tiny << endl;
    cout << "Value of sum B - 1: " << sumB - 1 << " should be the same as n * tiny: " << large_sum * tiny << endl;
    cout << "The values are not the same because for sum A, we are adding very small numbers to a relatively large number. This introduces more floating point errors than adding many tiny numbers together first and then adding a relatively large number as done in sum B." << endl;
}