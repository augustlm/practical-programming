#include<iostream>
#include<cmath>
#include<complex>

using namespace std;

int main()
{
    cout << "sqrt(complex<double>(-1, 0)) = " << sqrt(complex<double>(-1, 0)) << " vs i." << endl;
    cout << "sqrt(complex<double>(0, 1)) = " << sqrt(complex<double>(0, 1)) << " vs 0.707106781 + 0.707106781 i." << endl;
    cout << "exp(complex<double>(0, 1)) = " << exp(complex<double>(0, 1)) << " vs 0.540302306 + 0.841470985 i." << endl;
    cout << "exp(complex<double>(0, pi)) = " << exp(complex<double>(0, M_PI)) << " vs -1." << endl;
    cout << "pow(complex<double>(0, 1), complex<double>(0, 1)) = " << pow(complex<double>(0, 1), complex<double>(0, 1)) << " vs 0.20787957635." << endl;
    cout << "log(complex<double>(0, 1)) = " << log(complex<double>(0, 1)) << " vs 1.57079633 i." << endl;
    cout << "sin(complex<double>(0, pi)) = " << sin(complex<double>(0, M_PI)) << " vs 11.5487394 i." << endl;
}