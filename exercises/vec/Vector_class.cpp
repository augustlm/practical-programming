#include<iostream>
#include<cmath>
#include"Vector_class.hpp"

// Constructors

// Default Constructor: called if no variables passed
my_vector::my_vector()
    :   v_x (0)
    ,   v_y (0)
    ,   v_z (0)
{
}

// Std Constructor: called when variables passed
my_vector::my_vector(double x, double y, double z)
    :   v_x (x)
    ,   v_y (y)
    ,   v_z (z)
{
}

// Copy Constructor: creates deep copy of passed vector
my_vector::my_vector(const my_vector& other)
    :   v_x (other.v_x)
    ,   v_y (other.v_y)
    ,   v_z (other.v_z)
{
}

// Destructors

// Std Destructor
my_vector::~my_vector()
{
}

// Public member functions

// Print: prints the vector to terminal
void my_vector::print_vector() const
{
    std::cout << "(" << v_x << ", " << v_y << ", " << v_z << ")" << std::endl;
}

// Norm: returns the norm of some vector
double my_vector::norm() const
{
    return dot_product(*this);
}

// Dot product: returns the dot product of vector with other vector specified
double my_vector::dot_product(const my_vector& other) const
{
    return v_x * other.v_x + v_y * other.v_y + v_z * other.v_z;
}

// Cross product: calculates the cross product of vector with other vector specified
my_vector my_vector::cross_product(const my_vector& other) const
{
    double x = v_y * other.v_z - v_z * other.v_y;
    double y = v_z * other.v_x - v_x * other.v_z;
    double z = v_x * other.v_y - v_y * other.v_x;
    my_vector res(x, y, z);
    return res;
}

bool my_vector::approx(const my_vector& other, const double acc, const double eps) const
{
    if (!::approx(v_x, other.v_x))
    {
        return false;
    }
    else if (!::approx(v_y, other.v_y))
    {
        return false;
    }
    else if (!::approx(v_z, other.v_z))
    {
        return false;
    }
    else
    {
        return true;
    }
    
}

// Operators

// vec * double
my_vector operator*(const my_vector& vec_1, const double c)
{
    double x = vec_1.v_x * c;
    double y = vec_1.v_y * c;
    double z = vec_1.v_z * c;
    my_vector vec(x, y, z);
    return vec;
}

// double * vec
my_vector operator*(const double c, const my_vector& vec_1)
{
    my_vector vec = vec_1 * c;
    return vec;
}

// vec + vec
my_vector operator+(const my_vector& vec_1, const my_vector& vec_2)
{
    double x = vec_1.v_x + vec_2.v_x;
    double y = vec_1.v_y + vec_2.v_y;
    double z = vec_1.v_z + vec_2.v_z;
    my_vector vec(x, y, z);
    return vec;
}

// -vec
my_vector operator-(const my_vector& vec_1)
{
    double x = -vec_1.v_x;
    double y = -vec_1.v_y;
    double z = -vec_1.v_z;
    my_vector vec(x, y, z);
    return vec;
}

// vec - vec
my_vector operator-(const my_vector& vec_1, const my_vector& vec_2)
{
    double x = vec_1.v_x - vec_2.v_x;
    double y = vec_1.v_y - vec_2.v_y;
    double z = vec_1.v_z - vec_2.v_z;
    my_vector vec(x, y, z);
    return vec;
}

// overide the std::cout << operator to print vector
std::ostream& operator<<(std::ostream& strm, const my_vector& vec)
{
    return strm << "(" << vec.v_x << ", " << vec.v_y << ", " << vec.v_z << ")";
}

// Non member functions (could probably be moved to seperate file in larger project)

// Own comp function which takes floating point accuracy into account
bool approx(const double a, const double b, const double acc, const double eps)
{
    if (std::abs(a - b) < acc)
    {
        return true;
    }
    else if (std::abs(a - b) < (std::abs(a) + std::abs(b)) * eps)
    {
        return true;
    }
    else
    {
        return false;
    }
    
}