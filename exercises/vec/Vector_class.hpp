#pragma once
#ifndef VECTOR_CLASS_HPP_INCLUDED
#define VECTOR_CLASS_HPP_INCLUDED

class my_vector
{
private:

    // Define three cartesian coordinates
    double v_x;
    double v_y;
    double v_z;
    
public:

    // Define constructors
    my_vector();
    my_vector(double x, double y, double z);
    my_vector(const my_vector& other);

    // Define destructor
    ~my_vector();

    // Public member methods
    void print_vector() const;
    double norm() const;
    double dot_product(const my_vector& other) const;
    my_vector cross_product(const my_vector& other) const;
    bool approx(const my_vector& other, const double acc=1e-9, const double eps=1e-9) const;

    // Operators
    friend my_vector operator*(const my_vector& vec_1, const double c);
    friend my_vector operator*(const double c, const my_vector& vec_1);
    friend my_vector operator+(const my_vector& vec_1, const my_vector& vec_2);
    friend my_vector operator-(const my_vector& vec_1);
    friend my_vector operator-(const my_vector& vec_1, const my_vector& vec_2);
    friend std::ostream& operator<<(std::ostream& strm, const my_vector& vec);
};

// Non member functions (could probably be moved to seperate file in larger project)
bool approx(const double a, const double b, const double acc=1e-9, const double eps=1e-9);

#endif