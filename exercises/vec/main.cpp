#include<iostream>
#include"Vector_class.hpp"

using namespace std;

int main()
{
    cout << "Testing initialisers: " << endl;
    my_vector vec_1(1, 2, 3), vec_2(-3, 0, 4);
    cout << "v_1 = " << vec_1 << " and v_2 = " << vec_2 << endl;
    my_vector vec_1_copy(vec_1);
    cout << "initialise a copy of v_1. V_1_copy = " << vec_1_copy << endl;
    my_vector vec_default;
    cout << "initialise a default vector. V_default = " << vec_default << endl;
    cout << "\nTesting basic operators: " << endl;
    cout << "v_1 + v_2 = " << vec_1 + vec_2 << endl;
    cout << "v_1 - v_2 = " << vec_1 - vec_2 << endl;
    cout << "-v_1 = " << -vec_1 << endl;
    cout << "0.1 * v_1 = " << 0.1 * vec_1 << endl;
    cout << "v_2 * 4 = " << vec_2 * 4 << endl;
    cout << "\nTesting print member function: " << endl;
    cout << "v_1 = ";
    vec_1.print_vector();
    cout << "\nTesting dot product, cross product and norm member functions: " << endl;
    cout << "v_1 dot v_2 = " << vec_1.dot_product(vec_2) << endl;
    cout << "v_1 cross v_2 = " << vec_1.cross_product(vec_2) << endl;
    cout << "v_2 cross v_1 = " << vec_2.cross_product(vec_1) << endl;
    cout << "norm(v_1) = " << vec_1.norm() << endl;
    cout << "\nTesting approx function: " << endl;
    cout << "v_1 == v_1_copy = " << vec_1.approx(vec_1_copy) << endl;
    my_vector unit(1, 1, 1);
    unit = unit * 0.00001;
    vec_1_copy = vec_1_copy + unit;
    cout << "v_1_copy += 0.00001 * (1, 1, 1) = " << vec_1_copy << endl;
    cout << "v_1 == v_1_copy = " << vec_1.approx(vec_1_copy) << endl;
    cout << "\nThe overloaded toString method has been used to print so far. Considered tested. " << endl;
}